

#include "hadaq/api.h"
#include "dabc/Url.h"
int main() {

  std::string src = "localhost:6789";
  dabc::Url url(src);
  
  if (url.IsValid()) {
    if (url.GetProtocol().empty())
      src = std::string("mbss://") + src;
    
  }
  
  // hadaq::ReadoutHandle ref = hadaq::ReadoutHandle::Connect("localhost:6789");
  hadaq::ReadoutHandle ref = hadaq::ReadoutHandle::Connect(src.c_str());
  hadaq::RawEvent* evnt = 0;
  while(evnt = ref.NextEvent(1.)) {
    // any user code here
    //    evnt->Dump();

    std::cout << "Header " << evnt->GetSize() << " "
              << evnt->GetDecoding() << " "
              << evnt->GetId() << " "
              << evnt->GetSeqNr() << " "
              << evnt->GetRunNr() << " "
              << evnt->GetDate() << " "
              << evnt->GetTime() << " "
              << std::endl;
      
    hadaq::RawSubevent* sub = 0;
    while ((sub = evnt->NextSubevent(sub)) != 0) {
      
      //sub->Dump(true);
      unsigned ix = 0;
      unsigned len = 0xffffffff;
      unsigned prefix = 6;
  
      unsigned sz = ((sub->GetSize() - sizeof(hadaq::RawSubevent)) / sub->Alignment());
      
      if ((ix>=sz) || (len==0)) continue;
      if (ix + len > sz) len = sz - ix;
      
      unsigned wlen = 2;
      if (sz>99) wlen = 3; else
        if (sz>999) wlen = 4;
      
      for (unsigned cnt=0;cnt<len;cnt++,ix++)
        printf("%*s[%*u] %08x%s", (cnt%8 ? 2 : prefix), "", wlen, ix, (unsigned) sub->Data(ix), (cnt % 8 == 7 ? "\n" : ""));

  
      if (len % 8 != 0) printf("\n");
      
    }
      
  }
}

