#include "TUDPSocketHandler.hxx"
#include <iostream>

int TUDPSocketHandler::OpenUdpSocket(int server_port)
{
  int status;
   
  int fd = socket(AF_INET, SOCK_DGRAM, 0);
   
  if (fd < 0) {
    cm_msg(MERROR, "open_udp_socket", "socket(AF_INET,SOCK_DGRAM) returned %d, errno %d (%s)", fd, errno, strerror(errno));
    return -1;
  }

  int opt = 1;
  status = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

  if (status == -1) {
    cm_msg(MERROR, "open_udp_socket", "setsockopt(SOL_SOCKET,SO_REUSEADDR) returned %d, errno %d (%s)", status, errno, strerror(errno));
    return -1;
  }

  //  int bufsize = 8*1024*1024;
  int bufsize = 8*1024*1024;
  //int bufsize = 20*1024;

  status = setsockopt(fd, SOL_SOCKET, SO_RCVBUF, &bufsize, sizeof(bufsize));

  if (status == -1) {
    cm_msg(MERROR, "open_udp_socket", "setsockopt(SOL_SOCKET,SO_RCVBUF) returned %d, errno %d (%s)", status, errno, strerror(errno));
    return -1;
  }

  struct sockaddr_in local_addr;
  memset(&local_addr, 0, sizeof(local_addr));

  local_addr.sin_family = AF_INET;
  local_addr.sin_port = htons(server_port);
  local_addr.sin_addr.s_addr = INADDR_ANY;

  status = bind(fd, (struct sockaddr *)&local_addr, sizeof(local_addr));

  if (status == -1) {
    cm_msg(MERROR, "open_udp_socket", "bind(port=%d) returned %d, errno %d (%s)", server_port, status, errno, strerror(errno));
    return -1;
  }

  int xbufsize = 0;
  unsigned size = sizeof(xbufsize);

  status = getsockopt(fd, SOL_SOCKET, SO_RCVBUF, &xbufsize, &size);

  //printf("status %d, xbufsize %d, size %d\n", status, xbufsize, size);

  if (status == -1) {
    cm_msg(MERROR, "open_udp_socket", "getsockopt(SOL_SOCKET,SO_RCVBUF) returned %d, errno %d (%s)", status, errno, strerror(errno));
    return -1;
  }

    cm_msg(MINFO, "open_udp_socket", "UDP port %d socket receive buffer size is %d", server_port, xbufsize);

  socket_ = fd;
  return fd;
}


int TUDPSocketHandler::CheckUdp(int msec)
  {


    //struct timeval start, end;

    //gettimeofday(&start, NULL);

    int status;
    fd_set fdset;
    struct timeval timeout;

    FD_ZERO(&fdset);
    FD_SET(socket_, &fdset);

    timeout.tv_sec = msec/1000;
    timeout.tv_usec = (msec%1000)*1000;
    timeout.tv_sec = 0;
    timeout.tv_usec = 1;


    status = select(socket_+1, &fdset, NULL, NULL, &timeout);


    if (status < 0) {
      cm_msg(MERROR, "wait_udp", "select() returned %d, errno %d (%s)", status, errno, strerror(errno));
      return -1;
    }

    if (status == 0) {
      return 0; // timeout
    }


    //    gettimeofday(&end, NULL);

    //long seconds = (end.tv_sec - start.tv_sec);
    //long micros = ((seconds * 1000000) + end.tv_usec) - (start.tv_usec);

    //if(end.tv_usec %10000 == 0)    
    //printf("Time elpased in CheckUdp is %d seconds and %d micros\n", seconds, micros);

    if (FD_ISSET(socket_, &fdset)) {
      return 1; // have data
    }

    // timeout
    return 0;
  
  }

/* Swap bytes in 32 bit value.  */
#define _R__bswap_constant_32(x) \
  ((((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >>  8) |               \
   (((x) & 0x0000ff00) <<  8) | (((x) & 0x000000ff) << 24))




#define MAX_UDP_SZ (0x10000)

bool TUDPSocketHandler::ReadEvent(void *wp){


  //    struct timeval start, end;

  //  gettimeofday(&start, NULL);

  DWORD size_remaining_dwords, to_read_dwords, *pdata = (DWORD *)wp;

  // Read data from UDP socket
  struct sockaddr addr;
  socklen_t addr_len = sizeof(addr);
  char buf[MAX_UDP_SZ];
  int rd = recvfrom(socket_, buf, MAX_UDP_SZ, 0, &addr, &addr_len);
  
  if (rd < 0) {
    cm_msg(MERROR, "read_udp", "recvfrom() returned %d, errno %d (%s)", rd, errno, strerror(errno));
    return false;
  }
  
  //std::cout << "Data size: " << rd << " " << GetNumEventsInRB() <<std::endl;

  memcpy(pdata, buf, rd);

  // Do little endian to big endian conversion right away
  uint32_t* fData = reinterpret_cast<uint32_t*>(pdata);

  // check for correct endian-ness; if wrong, flip.
  // fourth word is endian encoding check word
  if(!((fData[3] & 0x1) == 1 && (fData[3] & 0x80000000) == 0)){
    //std::cout << "Endian swapping" << std::endl;
    for(int i = 0; i < rd/4; i++){
#if defined(OS_LINUX) && !defined(OS_DARWIN)
      fData[i] = __bswap_32 (fData[i]);
#else
      fData[i] = _R__bswap_constant_32(fData[i]);
#endif

    }

  }


  if(0)
    for(int i =0; i < 6; i++){
      printf("WWW 0x%x\n",pdata[i]);
    }
  
  // Let's save the number of words in first word
  pdata[0] = rd/4;
  //  printf("event size after 0x%x 0x%x\n",pdata[0],rd);

  rb_increment_wp(this->GetRingBufferHandle(), rd);
  
  this->IncrementNumEventsInRB(); //atomic

  //  gettimeofday(&end, NULL);
  
  //long seconds = (end.tv_sec - start.tv_sec);
  //long micros = ((seconds * 1000000) + end.tv_usec) - (start.tv_usec);
  
  //if(end.tv_usec %10000 == 0)    
  //  printf("Time elpased in read event is %d seconds and %d micros\n", seconds, micros);

  return true;

}


std::pair<uint32_t, uint32_t>  TUDPSocketHandler::FillEventBank(char *pevent){


  DWORD *src=NULL;
  DWORD *dest=NULL;

  int status = rb_get_rp(this->GetRingBufferHandle(), (void**)&src, 500);
  if (status == DB_TIMEOUT) {
    cm_msg(MERROR,"FillEventBank", "Got rp timeout for module %d", index_);
    printf("### num events: %d\n", this->GetNumEventsInRB());
    return std::pair<uint32_t, uint32_t>(9999999,8888888);
  }


  uint32_t size_copied = src[0];
  uint32_t size_words = size_copied;

  // >>> create data bank
  char bankName[5];
  snprintf(bankName, sizeof(bankName), "TRB%01i", index_);
  //  exit(0);

  bk_create(pevent, bankName, TID_DWORD, (void **)&dest);

  uint32_t limit_size = (32*422800-bk_size(pevent))/4; // what space is left in the event (in DWORDS)  
  if (size_words > limit_size) {
    //    printf("Event with size: %u (Module %02d) bigger than max %u, event truncated\n", size_words, this->GetModuleID(), limit_size);
    cm_msg(MERROR,"FillEventBank","Event with size: %u (Module %02d) bigger than max %u, event truncated", size_words, index_, limit_size);
  } 

  // Do some sanity checks...
  uint32_t triggerNumber = ((src[5] & 0xffffff00) >> 8);
  uint32_t epochWord = 0;
  if (size_copied > 10){    
    epochWord = (src[8] & 0xfffffff);
  }

  if(gLastEventNumber > 0){
    if(gLastEventNumber + 1 != triggerNumber && (gLastEventNumber != 0xffffff)){
      if(gNumberNonseqEventNumbers <= 20){
	cm_msg(MERROR, "read_event", "TRB (#%i) Non-sequential TRB3 event numbers: last=%i current=%i\n",index_,gLastEventNumber,triggerNumber);
	if ( gNumberNonseqEventNumbers == 20){
	  cm_msg(MERROR, "read_event", "TRB (#%i)More than 20 non-sequential event numbers: suppressing further messages\n", index_);
	}
      }
      gNumberNonseqEventNumbers++;
    }

  }
  gLastEventNumber = triggerNumber;

  // copy data over.
  memcpy(dest, src, size_copied*sizeof(uint32_t));

  this->DecrementNumEventsInRB(); //atomic
  rb_increment_rp(this->GetRingBufferHandle(), size_words*sizeof(uint32_t));

  //Close data bank
  bk_close(pevent, dest + size_copied);


  


  return std::pair<uint32_t, uint32_t>(triggerNumber,epochWord);


}

