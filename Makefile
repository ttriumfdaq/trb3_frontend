#
# Example Makefile for TRB3 frontend
#

CXX = g++ -std=c++11

CXXFLAGS = -g -O2 -Wall -Wuninitialized -I$(MIDASSYS)/include

# required ZLIB library

CXXFLAGS += -DHAVE_LIBZ
LIBS = -lm -lz -lutil   -lpthread -lssl -ldl -lrt 
LIB_DIR         = $(MIDASSYS)/linux/lib

# MIDAS library
MIDASLIBS = $(MIDASSYS)/linux/lib/libmidas.a

# fix these for MacOS
UNAME=$(shell uname)
ifeq ($(UNAME),Darwin)
MIDASLIBS = $(MIDASSYS)/darwin/lib/libmidas.a
LIB_DIR         = $(MIDASSYS)/darwin/lib
endif

# include files and library for trbnet communication with TRB3
CXXFLAGS +=  -I$(HOME)/trbsoft/trbnettools/include/
LIBS +=  $(HOME)/trbsoft/trbnettools/lib/libtrbnet.a

# These options only necessary if we use the original scheme of reading data from DABC event builder (as opposed to reading
# directly from the UDP socket).
# CXXFLAGS +=  -I$(HOME)/trbsoft/trb3/dabc/include/ 
# LIBS += -L$(HOME)/trbsoft/trb3/dabc/lib/ -lDabcHadaq -lDabcBase 


OBJS:= TUDPSocketHandler.o

all: $(OBJS) fetrb3UDP.exe fetrb3Multi.exe #  fetrb3.exe  trb3_test.exe 

trb3_test.exe: trb3_test.cxx $(OBJS) 
	$(CXX) -o $@ $(CXXFLAGS) $^ $(LIBS) $(ROOTGLIBS) -lm -lz -lpthread -lssl -lutil

fetrb3.exe:  %.exe:   %.o 
	$(CXX) -o $@ $(CXXFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIB_DIR)/mfe.o $(MIDASLIBS) $(LIBS)

fetrb3UDP.exe:  %.exe:   %.o 
	$(CXX) -o $@ $(CXXFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIB_DIR)/mfe.o $(MIDASLIBS) $(LIBS)

fetrb3Multi.exe:  %.exe:   %.o $(OBJS)
	$(CXX) -o $@ $(CXXFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIB_DIR)/mfe.o $(MIDASLIBS) $(LIBS)

%.o: %.cxx
	$(CXX) $(CXXFLAGS) $(OSFLAGS) -c $<

clean::
	-rm -f *.o *.a
	-rm -f *.exe
	-rm -rf *.exe.dSYM
