/********************************************************************\
MIDAS frontend for reading out TRB3 FPGA-TDC
 Thomas Lindner (TRIUMF)
\********************************************************************/


#include <vector>
#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include "midas.h"
#include <stdint.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <signal.h>

// Headers for TRB3 software packages.
#include <sys/socket.h>   
#include "hadaq/api.h"
#include "dabc/Url.h"

#include <trbnet.h>
#include <trberror.h>

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fetrb3";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size =  3 * 1024 * 1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) --- not really used here */
INT max_event_size_frag = 2 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 20 * 1000000;
 void **info;
char  strin[256];
HNDLE hDB, hSet;

// buffer for TRB3 commands
static size_t BUFFER_SIZE = 4194304;  /* 4MByte */
static uint32_t buffer[4194304];    
  
/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trb3_event(char *pevent, INT off);

INT read_trb3_temperature(char *pevent, INT off);

/*-- Equipment list ------------------------------------------------*/

#undef USE_INT

EQUIPMENT equipment[] = {

   {"TRB3",               /* equipment name */
    {1, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_POLLED,              /* equipment type */
     LAM_SOURCE(0, 0xFFFFFF),        /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING,           /* read only when running */
     1000,                    /* poll for 1000ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trb3_event,      /* readout routine */
    },
   {"TRB3_TEMP",               /* equipment name */
    {2, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC,              /* equipment type */
     LAM_SOURCE(0, 0xFFFFFF),        /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_ODB|RO_ALWAYS,           /* read only when running */
     2000,                    /* poll for 1000ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     1,                      
     "", "", "",},
    read_trb3_temperature,      /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/



#define TRB3DAQSETTINGS_STR(_name) const char *_name[] = {  \
"[.]",\
"Enable Calibration = INT : 1",\
"Only Calibrated = INT : 1", \
"UseWindowing = INT : 1", \
"FGPA1Enable = BOOL[4] :", \
"[0] y",		   \
"[1] n",		   \
"[2] n",		   \
"[3] n",		   \
"FGPA2Enable = BOOL[4] :", \
"[0] n",		   \
"[1] n",		   \
"[2] n",		   \
"[3] n",		   \
"FGPA3Enable = BOOL[4] :", \
"[0] n",		   \
"[1] n",		   \
"[2] n",		   \
"[3] n",		   \
"FGPA4Enable = BOOL[4] :", \
"[0] n",					\
"[1] n",					\
"[2] n",					\
"[3] n",					\
"",\
NULL }

TRB3DAQSETTINGS_STR(trb3daqsettings_str);


struct TRB3DAQSETTINGS {
  INT EnableCalibration;   
  INT OnlyCalibrated;
  INT UseWindowing;
  BOOL FPGA1Enable[4];
  BOOL FPGA2Enable[4];
  BOOL FPGA3Enable[4];
  BOOL FPGA4Enable[4];
  
} static config_global;

HNDLE settings_handle_global_; 

// TRB3 event builder readout handler
hadaq::ReadoutHandle globalReadoutHandle;

hadaq::RawEvent* globalEvent;

int lastEventSeqNr = 0;
int numSeqErrors = 0;

// This is the list of FPGA IDs for our TRB3
int fpga_ids[4] = {0x100,0x101,0x102,0x103};



// enable/disable block of channels. setclr = true means enable the channel. 
int setEnableBlock(int board, int channel, bool setclr){
  
  int registr = (int)(channel/2) + 0xc802;
  int regvalue;
  if(channel%2)
    regvalue = 0xffff0000;
  else
    regvalue = 0xffff;

  int status;
  if(setclr)
    status = trb_register_setbit(board,registr,regvalue);
  else
    status = trb_register_clearbit(board,registr,regvalue);

  
  if(status == -1)   cm_msg(MERROR,"setup_trb","Problem with setting enable bit ");
  if (trb_errno == TRB_STATUS_WARNING) cm_msg(MERROR,"setup_trb","Status-Bit(s) have been set:\n%s\n", trb_termstr(trb_term));

  return 0;
}


INT setup_trb(){
  
  int size = sizeof(TRB3DAQSETTINGS); 
  int status = db_get_record(hDB, settings_handle_global_, &config_global, &size, 0);
  std::cout << "Current settings: EnableCalib = "<< config_global.EnableCalibration
            << " ; OnlyCalib = " << config_global.OnlyCalibrated
            << " ; UseWindowing = " << config_global.UseWindowing 
            << std::endl;

  
  // enable/disable the event window (get TDC for some/all hits)
  if(config_global.UseWindowing){
    for(int i = 0; i < 4; i++)
      status = trb_register_setbit(fpga_ids[i],0xc801,0x80000000);
  }else{
    for(int i = 0; i < 4; i++)
      status = trb_register_clearbit(fpga_ids[i],0xc801,0x80000000);
  }
  if(status == -1)   cm_msg(MERROR,"setup_trb","Problem with setting window bit ");
  if (trb_errno == TRB_STATUS_WARNING) cm_msg(MERROR,"setup_trb","Status-Bit(s) have been set:\n%s\n", trb_termstr(trb_term));

  // enable/disable particular FPGA channels.
  for(int i = 0; i < 4; i++){// loop over FPGA
    std::cout << "Enable for FPGA" << i << "; ";
    for(int j = 0; j < 4; j++){
      bool value;
      if(i == 0) value = config_global.FPGA1Enable[j]; 
      else if(i == 1)  value = config_global.FPGA2Enable[j]; 
      else if(i == 2)  value = config_global.FPGA3Enable[j]; 
      else if(i == 3)  value = config_global.FPGA4Enable[j]; 
      
      std::cout << value << " ";
      setEnableBlock(fpga_ids[i], j, value);
    }

    std::cout << std::endl;
    
  }
  
  for(int i = 0; i < 4; i++){
    trb_register_read(fpga_ids[i],0,buffer,BUFFER_SIZE);
    float temp = (float)((buffer[1] & 0xfff00000) >> 20)/16.0;
    cm_msg(MINFO,"frontend_init","temperature for FPGA=%i is %fC.",i,temp);
  }
  
  cm_msg(MINFO,"frontend_init","TRB3 Board Setup Finished");

  return 0;
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{

  int status=0;

 //// Create the setting directory if necessary
 //trb3daqsettings_str
 status = db_create_record(hDB,0,"/Equipment/TRB3/Settings",strcomb(trb3daqsettings_str));

 // Get the handle
 status = db_find_key (hDB, 0, "/Equipment/TRB3/Settings", &settings_handle_global_);
 if (status != DB_SUCCESS) cm_msg(MINFO,"frontend_init","Key not found. Return code: %d",  status);
   
 // Get the settings
 int size = sizeof(TRB3DAQSETTINGS); 
 status = db_get_record(hDB, settings_handle_global_, &config_global, &size, 0);


 // Setup trbnet connection.
 init_ports(); 

 unsigned int i;
 uint16_t trb_address;
 trb_address = 0xffff;
 cm_msg(MINFO,"frontend_init","Reading addresses of TRB3s (central FPGA + 4 TDC FPGAs:");
 
 status = trb_read_uid(trb_address, buffer, BUFFER_SIZE);     
 if (status == -1) {    
   printf("read_uid failed.  Error message: %s\n",   
            trb_strerror());       
   return -1;     
 } else {      
   for (i = 0; i < status; i += 4) {     
     cm_msg(MINFO,"frontend_init","0x%04x  0x%08x%08x  0x%02x",           
                                    buffer[i + 3],
                                    buffer[i], buffer[i + 1], buffer[i + 2]);
   }   
 }     
 setup_trb();
 
 // Setup connection to TRB3 event builder stream
 
 std::string src = "localhost:6789";
 dabc::Url url(src);
  
  if (url.IsValid()) {
    if (url.GetProtocol().empty())
      src = std::string("mbss://") + src;
    
  }
 globalReadoutHandle = hadaq::ReadoutHandle::Connect(src.c_str());
 if(!globalReadoutHandle.null())
   cm_msg(MINFO,"frontend_init","TRB3 Stream Initialized");
 else{
   cm_msg(MERROR,"frontend_init","Problem initializing TRB3 Stream. Exiting. ");
   return  FE_ERR_HW;
 }

 globalEvent = 0;
 lastEventSeqNr = 0;
 numSeqErrors = 0;



 
 cm_msg(MINFO,"frontend_init","Finished Initializating TRB3 Frontend");


 return SUCCESS;
}




/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  printf("Exiting fetrb3!\n");
   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
// Upon run stasrt, read ODB settings and write them to DCRC
INT begin_of_run(INT run_number, char *error)
{
  lastEventSeqNr = 0;
  numSeqErrors = 0;
  
  setup_trb();
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
  usleep(100);
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/
// Not currently used for DCRC readout
extern "C" { INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;

   for (i = 0; i < count; i++) {
   
     globalEvent = globalReadoutHandle.NextEvent(0.1);
     if(globalEvent)
       if (!test)
         return 1;
   }

   usleep(100);
   return 0;
}
}

/*-- Interrupt configuration ---------------------------------------*/
// This is not currently used by the DCRC readout
extern "C" { INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}
}

#include <sys/time.h>
/*-- Event readout -------------------------------------------------*/
INT read_trb3_event(char *pevent, INT off)
{

  /* init bank structure */
  bk_init32(pevent);
  
  uint32_t *pdata32;
  /* create structured TRB0 bank of double words (i.e. 4-byte words)  */
  bk_create(pevent, "TRB0", TID_DWORD, (void **)&pdata32);



  if(0)
    std::cout << "Header " << globalEvent->GetSize() << " "
            << globalEvent->GetDecoding() << " "
            << globalEvent->GetId() << " "
            << globalEvent->GetSeqNr() << " "
            << globalEvent->GetRunNr() << " "
            << globalEvent->GetDate() << " "
            << globalEvent->GetTime() << " "
            << std::endl;
  
  // Write the bank header
  // conains: size, decoding, id, seq nr,
  // run nr, date, time
  *pdata32++ = globalEvent->GetSize();
  *pdata32++ = globalEvent->GetDecoding();
  *pdata32++ = globalEvent->GetId();
  *pdata32++ = globalEvent->GetSeqNr();
  *pdata32++ = globalEvent->GetRunNr();
  *pdata32++ = globalEvent->GetDate();
  *pdata32++ = globalEvent->GetTime();

  // Sanity check.  Make sure we aren't missing events, that event numbers are sequential.
  if(globalEvent->GetSeqNr()-lastEventSeqNr != 1 && lastEventSeqNr != 0){
    numSeqErrors++;
    if(numSeqErrors < 10){ 
      cm_msg(MERROR,"read_trb3_event","TRB3 sequence numbers not sequential: %x %x",
             globalEvent->GetSeqNr(),lastEventSeqNr);
    }else if(numSeqErrors%100000 == 0){
      cm_msg(MERROR,"read_trb3_event","TRB3 sequence numbers not sequential: %x %x (only print 1 out of 10,000 errors)",
             globalEvent->GetSeqNr(),lastEventSeqNr);
    }

  }
  lastEventSeqNr = globalEvent->GetSeqNr();
  
  // Now write all sub-event information...
  // This loop is mostly stolen from hadaq::RawSubevent::PrintRawData()
  hadaq::RawSubevent* sub = 0;
  while ((sub = globalEvent->NextSubevent(sub)) != 0) {

    //printf("Subevent ID 0x%x\n",sub->GetId());
    unsigned ix = 0;
    unsigned len = 0xffffffff;
    
    unsigned sz = ((sub->GetSize() - sizeof(hadaq::RawSubevent)) / sub->Alignment());
    
    if ((ix>=sz) || (len==0)) continue;
    if (ix + len > sz) len = sz - ix;
    
    for (unsigned cnt=0;cnt<len;cnt++,ix++){
      //printf("[%*u] %08x%s", "", ix, (unsigned) sub->Data(ix), (cnt % 8 == 7 ? "\n" : ""));
      *pdata32++ = (unsigned) sub->Data(ix);
    }
    
  }
  

  int size2 = bk_close(pevent, pdata32 );    
  
  return bk_size(pevent);  
  

}

INT read_trb3_temperature(char *pevent, INT off){

  /* init bank structure */
  bk_init32(pevent);
  
  float *pdata;
  bk_create(pevent, "TRTM", TID_FLOAT, (void **)&pdata);

  trb_register_read(0xc001,0,buffer,BUFFER_SIZE);
  float temp = (float)((buffer[1] & 0xfff00000) >> 20)/16.0;
  *pdata++ = temp;
  printf("central FPGA temperature: %f\n",temp);
  for(int i = 0; i < 4; i++){
    trb_register_read(fpga_ids[i],0,buffer,BUFFER_SIZE);
    temp = (float)((buffer[1] & 0xfff00000) >> 20)/16.0;
    *pdata++ = temp;    
  }

  int size2 = bk_close(pevent, pdata );    
  
  return bk_size(pevent);
}


