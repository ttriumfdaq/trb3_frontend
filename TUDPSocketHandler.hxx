#ifndef TUDPSocketHandler_seen
#define TUDPSocketHandler_seen

#include <vector>
#include <atomic>
#include "midas.h"
#include "msystem.h"

// Class that takes care of collecting data from socket connection,
// placing data into ring buffer 
// and then taking data out of ring buffer into event.
class TUDPSocketHandler {

public:
  TUDPSocketHandler(int index): num_events_in_rb_(0) {
    index_ = index;
  };

  // Need non-default move constructor because of atomic variable.
  TUDPSocketHandler(TUDPSocketHandler&& other) noexcept
  : index_(std::move(other.index_)),
    num_events_in_rb_(other.num_events_in_rb_.load())
  {
    rb_handle_ = std::move(other.rb_handle_);
    socket_ = std::move(other.socket_);
  };


  // Open the UDP socket...
  int OpenUdpSocket(int server_port);

  // Check if there is UDP data...
  int CheckUdp(int msec);


  void begin_of_run(){
    gLastEventNumber = -1;
    gNumberNonseqEventNumbers = 0;
  }

  void end_of_run(){
    if(gNumberNonseqEventNumbers > 0){
      cm_msg(MERROR, "TRB_UDP::EOR", "Number of non-sequential events in run for TRB %i was %i",index_, gNumberNonseqEventNumbers );
    }
    num_events_in_rb_=0;
  }


  bool CheckEvent();
  bool ReadEvent(void *);
  std::pair<uint32_t, uint32_t>  FillEventBank(char *);

  //int Connect(){ };
  //bool Disconnect(){};
  //  bool Poll(DWORD*){};

  void SetRingBufferHandle(int rb_handle) { //! set ring buffer index
    rb_handle_ = rb_handle;
  }
  int GetRingBufferHandle() {             //! returns ring buffer index
    return rb_handle_;
  }
  int GetNumEventsInRB() {                //! returns number of events in ring buffer
    return num_events_in_rb_.load();
  }

  /* These are atomic with sequential memory ordering. See below */
  void IncrementNumEventsInRB() {         //! Increment Number of events in ring buffer
    num_events_in_rb_++;
  }
  void DecrementNumEventsInRB() {         //! Decrement Number of events in ring buffer
    num_events_in_rb_--;
  }
  void ResetNumEventsInRB() {             //! Reset Number of events in ring buffer
    num_events_in_rb_=0;
  }

private:

  int rb_handle_;         //!< Handle to ring buffer
  std::atomic<int> num_events_in_rb_;

  int index_;

  int socket_;

  // Reset sanity check variables;                                                                                                                                              // Event number checks
  int gLastEventNumber;
  int gNumberNonseqEventNumbers ;


};



#endif

