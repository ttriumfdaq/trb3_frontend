#include <stdio.h>
#include <iostream>

#include "TRootanaDisplay.hxx"
#include "TH1D.h"
#include "TV792Data.hxx"

#include "TFancyHistogramCanvas.hxx"
#include "TSimpleHistogramCanvas.hxx"
#include "TInterestingEventManager.hxx"


#include "TAnaManager.hxx"
#include "TTRB3DataV2.hxx"
#include "TReferenceChannelCanvas.hxx"

class MyTestLoop: public TRootanaDisplay { 

public:
	
  // An analysis manager.  Define and fill histograms in 
  // analysis manager.
  TAnaManager *anaManager;

  MyTestLoop() {
    SetOutputFilename("example_output");
    DisableRootOutput(false);
    anaManager = new TAnaManager();
    // Number of events to skip before plotting one.
    //SetNumberSkipEvent(10);
    // Choose to use functionality to update after X seconds
    SetOnlineUpdatingBasedSeconds();
    // Uncomment this to enable the 'interesting event' functionality.
    //iem_t::instance()->Enable();
  }

  void AddAllCanvases(){

    // Set up tabbed canvases
        
    if(anaManager->HaveTRB3Histograms()){

      AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetTRB3Histograms(),"TRB3 Time"),"TRB3");
      AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetTRB3FineHistograms(),"TRB3 Fine"),"TRB3");
      AddSingleCanvas(new TReferenceChannelCanvas(anaManager->GetTRB3DiffHistograms(),"TRB3 Diff"),"TRB3");
      AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetPMTDisplay(),"PMT Display Cumul"),"ARICH");
      AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetPMTNhitsDisplay(),"PMT Nhits Display"),"ARICH");
      AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetPMTNhChanDisplay(),"PMT Nhits Channel Display"),"ARICH");
      AddSingleCanvas(new TReferenceChannelCanvas(anaManager->GetTRB3DiffTrigHistograms(),"TRB3 Diff Triggered"),"ARICH");
      AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetPMTNhChanDisplay(),"PMT Nhits Channel Display"));
      AddSingleCanvas(new TReferenceChannelCanvas(anaManager->GetTRB3DiffTrigHistograms(),"TRB3 Diff Triggered"));
      AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetTRB3Hits(),"TRB3 Total Hits"),"TRB3");
      AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetTRB3NHits(),"TRB3 Average Hits"),"TRB3");

    }

    AddSingleCanvas(new TSimpleHistogramCanvas(anaManager->fPMTDisplayHistEvent,"PMT Display Single","COLZ"),"PMT Display");
    AddSingleCanvas(new TSimpleHistogramCanvas(anaManager->fHitsVsTOF,"nhits vs TOF","COLZ"));
    
    SetDisplayName("TRB3 Display");
  };

  
  bool CheckOption(std::string option){

    bool value = CheckTRBOption(option);
    //    if(value){

    //}
    
    return value;
  }
  
  virtual ~MyTestLoop() {};

  void BeginRun(int transition,int run,int time) {
    std::cout << "User BOR method" << std::endl;
    anaManager->BeginRun(transition, run, time);
  }

  void EndRun(int transition,int run,int time) {
    std::cout << "User EOR method" << std::endl;
    anaManager->EndRun(transition, run, time);
  }

  void ResetHistograms(){}

  void UpdateHistograms(TDataContainer& dataContainer){
    anaManager->ProcessMidasEvent(dataContainer);
  }

  void PlotCanvas(TDataContainer& dataContainer){}


}; 






int main(int argc, char *argv[])
{
  MyTestLoop::CreateSingleton<MyTestLoop>();  
  return MyTestLoop::Get().ExecuteLoop(argc, argv);
}

