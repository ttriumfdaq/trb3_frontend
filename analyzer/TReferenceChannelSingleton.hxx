#ifndef TReferenceChannelSingleton_h
#define TReferenceChannelSingleton_h


class TReferenceChannelSingleton
{
public:
  
  static TReferenceChannelSingleton *instance();

  int  GetReferenceModule(){return fReferenceModule;};
  void SetReferenceModule(int mod){fReferenceModule = mod;};
  int  GetReferenceChannel(){return fReferenceChannel;};
  void SetReferenceChannel(int ch){fReferenceChannel = ch;};
  
private:
  
  // pointer to global object
  static TReferenceChannelSingleton *s_instance;

  // hidden private constructor
  TReferenceChannelSingleton();
  
  int fReferenceChannel;
  int fReferenceModule;
  
};

//#define  iem_t  TInterestingEventManager;
// Setup a short name for class IEM = Interesting Event Manager
//typedef TInterestingEventManager iem_t;

#endif
