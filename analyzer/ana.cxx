// Default program for dealing with various standard TRIUMF VME setups:
// V792, V1190 (VME), L2249 (CAMAC), Agilent current meter
//
//

#include <stdio.h>
#include <iostream>
#include <time.h>

#include "TRootanaEventLoop.hxx"
#include "TAnaManager.hxx"
#include "TTRB3DataV2.hxx"

class Analyzer: public TRootanaEventLoop {




public:

  // An analysis manager.  Define and fill histograms in 
  // analysis manager.
  TAnaManager *anaManager;
  
  Analyzer() {
    //DisableAutoMainWindow();
    UseBatchMode();
    anaManager = 0;
    
  };

  virtual ~Analyzer() {};

  void Initialize(){

#ifdef HAVE_THTTP_SERVER
    std::cout << "Using THttpServer in read/write mode" << std::endl;
    SetTHttpServerReadWrite();
#endif

  }

  
  bool CheckOption(std::string option){
    return CheckTRBOption(option);
  }

  void InitManager(){
    
    if(anaManager)
      delete anaManager;
    //if(!anaManager)
      anaManager = new TAnaManager();
    
  }
  
  
  void BeginRun(int transition,int run,int time){
    
    InitManager();
    
  }


  bool ProcessMidasEvent(TDataContainer& dataContainer){

    if(!anaManager) InitManager();
    
    anaManager->ProcessMidasEvent(dataContainer);
    return true;
    TTRB3DataV2 *data = dataContainer.GetEventData<TTRB3DataV2>("TRB0");
    if(!data) data = dataContainer.GetEventData<TTRB3DataV2>("TRBA");
    bool printout = false;
    if(0){

      std::cout << "Size "<< data->GetSize() << std::endl;      
      for(int i = 0; i < data->GetSize()/4;i++)
	printf("0x%08x %i\n",data->GetData32()[i],i); 
      std::cout << "Keep going " << std::endl;
      //continue;


      if(0){
      int numberPulses = 0;
      bool good1 = false, good2 = false;
      int ref1id = 0;
      for(int i = 0; i < data->GetNumberMeasurements(); i++){
	std::vector<TrbTdcMeasV2> meas =  data->GetMeasurements();
	double time = meas[i].GetFinalTime();
	uint32_t id = meas[i].GetBoardId();
	uint32_t ch = meas[i].GetChannel();
	if(id == 2 && (ch == 1 ) ){ good1 = true; ref1id = i;}
	if(id == 2 && (ch == 2 ) ) good2 = true;
      }

      for(int i = 0; i < data->GetNumberMeasurements(); i++){
	std::vector<TrbTdcMeasV2> meas =  data->GetMeasurements();
	double time = meas[i].GetFinalTime();
	uint32_t id = meas[i].GetBoardId();
	uint32_t ch = meas[i].GetChannel();
	if(id == 2 && (ch == 1 || ch == 2) && good1 && good2){
	  std::cout <<" TDC (FPGA=" << id << ", ch=" << ch << ") is " << time/1000.0 << " ns. Coarse time :" << meas[i].GetCoarseTime()
		    << " fine time: " << meas[i].GetFineTime()<< " isLeading: " << meas[i].IsLeading() << std::endl;
	    //<< " " <<  std::hex << meas[i].GetTDCWord() << std::dec << std::endl;
	  if(ch == 2){

	    std::cout << "Diff = " << (time-meas[ref1id].GetFinalTime())/1000.0 << "ns" << std::endl;
	  }
	  printout = true;
	}
      }
      }
    }
    if(printout)    
    std::cout << "Data for new event." << std::endl;


    return true;
  }


}; 


int main(int argc, char *argv[])
{

  Analyzer::CreateSingleton<Analyzer>();
  return Analyzer::Get().ExecuteLoop(argc, argv);

}

