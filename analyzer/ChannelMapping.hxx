//Class to define TRB3. Used for channel mapping/
// Andrew Sikora, Blair Jamieson
// June 12, 2019

#ifndef ChannelMapping_h
#define ChannelMapping_h

#include <iostream>
#include <fstream>
#include <string>

int ChannelsPerConnection = 16;
std::vector<int> trbids;
std::vector<int> tdcids;
std::vector<int> pmtids;
std::vector<int> pmtcons;
std::vector<int> tdcCons;
std::vector<int> globXs;
std::vector<int> globYs;

class PMT {
public: 
  int get_PMT_Connector(){return PMTConnector;}

private:
  int PMTConnector;
};

struct ConPairing{
  int tdcCon;
  int PMTCon;
};

class TDC {
public:
  int get_tdc_id(){return tdcid;}
  TDC(int id, std::vector<ConPairing>* pairs): tdcid(id), tdcToPMT(*pairs) {}
  std::vector<ConPairing>* GetPairingVector(){return &tdcToPMT;}

private:
  int tdcid;
  std::vector<ConPairing> tdcToPMT;
};


class TRB3 {
public:
  int get_trb_id(){return trbid;}
  TRB3(int id, std::vector<TDC>* tdcvec): trbid(id), tdc(*tdcvec){}
  std::vector<TDC>* GetTDCVector(){return &tdc;}

private:
  int trbid;
  std::vector<TDC> tdc;
};

std::vector<TRB3> trbvec;

void GetTRBConfig(std::string filename);

double GetXCoord(int Evtrbid, int Evtdcid, int EvtdcCon, int EvCh);
double GetYCoord(int Evtrbid, int Evtdcid, int EvtdcCon, int EvCh);


#endif
