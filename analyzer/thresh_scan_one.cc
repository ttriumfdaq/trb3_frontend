{

   int run = 204;
   int threshold = -100;
   int number_of_files = 33;

   int channel = 1;
   char *filename[50];
   
   TChain trb3_chain("midas_data");
   TChain thresh_chain("threshold_times");

   for(int i=0; i < number_of_files; i++){
     if(i < 10){
       filename[i] = Form("output_000204_000%i.root",i);       
       trb3_chain.Add(filename[i]);
       thresh_chain.Add(filename[i]);
     }
     if(i < 100 and i >= 10){
       filename[i] = Form("output_000204_00%i.root",i);
       trb3_chain.Add(filename[i]);
       thresh_chain.Add(filename[i]);
     }
   }
   
   

   
   TGraphErrors *gr[1024];
   TGraphErrors *gr_time[1024];
   TH1D *hist_time[1024];
   TH2D *channel_plot = new TH2D("channel_plot", "channel_plot",20,-10,10,20,-10,10);
   TH2D *ChannelVsThreshold = new TH2D("ChannelVsThreshold","ChannelVsThreshold",40,24107,44107,1024,0,1024);
   TH2D *TimeDiffVsFPGA = new TH2D("TimeDiffVsFPGA","TimeDiffVsFPGA",16,0,16,2000,-1000,1000);
   for(int i=0; i<1024; i++) gr[i] = new TGraphErrors();
   for(int i=0; i<1024; i++) gr_time[i] = new TGraphErrors();
   for(int i=0; i<1024; i++) hist_time[i] = new TH1D(Form("hist_time_%d",i),"",1000,-500,500);


   Int_t           timestamp;
   Int_t           serialnumber;
   Int_t           trb_hits;
   Int_t           threshold_step = 500; 
   double          trb_hit_ref_time[36]; // Stores the reference time of each FPGA 
   double          trb_hit_time[50000];   //[trb_hits]
   double          trb_hit_epoch[50000];
   double          trb_hit_final_time[50000]; // [trb_hits]
   Int_t           trb_hit_fpga[50000];   //[trb_hits]
   Int_t           trb_hit_ch[50000];   //[trb_hits]
   Int_t           trb_hit_x_bin[50000];   //[trb_hits]
   Int_t           trb_hit_y_bin[50000];   //[trb_hits]
   double          threshold_time[50000];
   Int_t           padiwa_threshold[50000];
   double          threshold_now[1024];
   double          threshold_previous[1024];
   double          threshold_change_timestamp[1024];
   double          thresholds[1024];
   int             y_bin;
   int             x_bin;

   //if(tt==NULL){
   //thresh+=interval;
   //continue;
   //}

   thresh_chain.SetBranchAddress("threshold_time",threshold_time);
   thresh_chain.SetBranchAddress("padiwa_threshold",padiwa_threshold);
   
   trb3_chain.SetBranchAddress("trb_hits",&trb_hits);
   trb3_chain.SetBranchAddress("trb_hit_ch",trb_hit_ch);
   trb3_chain.SetBranchAddress("trb_hit_fpga",trb_hit_fpga);
   trb3_chain.SetBranchAddress("trb_hit_time",trb_hit_time);
   trb3_chain.SetBranchAddress("trb_hit_epoch",trb_hit_epoch);
   trb3_chain.SetBranchAddress("trb_hit_final_time",trb_hit_final_time);
   trb3_chain.SetBranchAddress("trb_hit_x_bin",trb_hit_x_bin);
   trb3_chain.SetBranchAddress("trb_hit_y_bin",trb_hit_y_bin);

   int num_thresh_changes = 0;
   bool thresh_changed = false;

   Int_t trb3_chain_entries = (Int_t)(trb3_chain.GetEntries());
   Int_t thresh_chain_entries = (Int_t)(thresh_chain.GetEntries());

   std::cout << "TRB3 Chain Entries: " << trb3_chain_entries << ", Threshold Chain Entries: " << thresh_chain_entries << std::endl;

   for(int i=0; i<thresh_chain_entries; i++){
     thresh_chain.GetEntry(i);
     thresh_changed = false;
     if (i == 0){
       for(int j = 0; j < 1024; j++){
	 threshold_previous[j] = padiwa_threshold[j];
       }
     }
     else{
       for(int j=0; j < 1024; j++){
	 threshold_now[j] = padiwa_threshold[j];
	 //std::cout << j << " " << threshold_now[j] << std::endl;
	 if (threshold_now[j] == 0){
	   continue;
	 }
	 else{
	   
	   //std::cout << "Channel 0 Threshold: " << threshold_now[j] << std::endl;
	   
	   if (threshold_now[j] <= (threshold_previous[j] - threshold_step) and (threshold_now[j] > (threshold_previous[j] - 1.001*threshold_step)) and threshold_now[j] < 50000 and threshold_previous[j] < 50000){
	     if (thresh_changed == false and (j%64 == 0)){
	       
	       std::cout << "j: " << j << ", Thresh Now: " << threshold_now[j] << ", Threshold Previously: " << threshold_previous[j] << ", Associated Final Time: " << threshold_time[j] << std::endl;
	       
	       threshold_change_timestamp[num_thresh_changes] = threshold_time[j];
	       thresholds[num_thresh_changes] = threshold_previous[j];
	       //std::cout << "Threshold Change Time: " << threshold_time[j] << std::endl;
	       thresh_changed = true ;
	       num_thresh_changes = num_thresh_changes + 1;
	       thresholds[num_thresh_changes] = threshold_now[j];
	     }
	   }
	   
	   threshold_previous[j] = threshold_now[j];
	 }
       }
     }
   }
   
   std::cout << "Number of Threshold Changes: " << num_thresh_changes << std::endl;
   
   int nhits[5000][5000];
   int nhits_time[5000][5000];
   double times[5000][5000];
   int number_of_skips = 0; //
   for(int j=0; j<5000;j++){
     for(int i=0; i<5000; i++){
       nhits[i][j]=0;
       nhits_time[i][j]=0;
     }
   }
   for(int i=0; i<trb3_chain_entries; i++){
     trb3_chain.GetEntry(i);
     //std::cout << "i: " << i << std::endl;
     //std::cout << "Number of hits " << trb_hits << std::endl;
     for(int j=0; j<trb_hits; j++){
       if(trb_hit_ch[j] == 0)trb_hit_ref_time[trb_hit_fpga[j]] = trb_hit_time[j];
     }
       
     for(int j=0; j<trb_hits; j++) {
       
       if ((trb_hit_final_time[j] >= threshold_change_timestamp[number_of_skips] - 5*(pow(10.0,10.0))) and (trb_hit_final_time[j] <= threshold_change_timestamp[number_of_skips])){
	 //std::cout << "Skipped this hit" << std::endl;
       }
       if (trb_hit_final_time[j] > threshold_change_timestamp[number_of_skips] and threshold_change_timestamp[number_of_skips] != 0){
	 number_of_skips++;
	 //std::cout << "Number of Skips: " << number_of_skips << std::endl;
       }
       else{
	 //if(trb_hit_ch[j]>64 || trb_hit_ch[j]<0) std::cout << "  hit channel: " << trb_hit_ch[j] << std::endl;
	 int fpga = trb_hit_fpga[j];
	 int ch = trb_hit_ch[j];
       
	 
	 double tdiff = (trb_hit_time[j]-trb_hit_ref_time[fpga])/1000.;
	 if(tdiff < 1000 and tdiff > -1000 and tdiff != 0){
	  
	   //std::cout << "Hit "<< j << ": FPGA " <<  fpga << ", Channel "<<  trb_hit_ch[j] << ", Hit Time " << trb_hit_time[j] << " , Final Time: " << trb_hit_final_time[j] << std::endl;
	   //std::cout << "Final Time: " << trb_hit_final_time[j] << std::endl;
	   //std::cout << "t0: " << trb_hit_ref_time[fpga]<< ", tdiff: " << tdiff << ", trb_hit_time: " << trb_hit_time[j]<< std::endl;
	   
	   nhits[trb_hit_ch[j]][number_of_skips]++;	
	   nhits_time[trb_hit_ch[j]][number_of_skips]++;
	   hist_time[trb_hit_ch[j]]->Fill((trb_hit_time[j]-trb_hit_ref_time[fpga])/1000.);
	   if(ch != 0){
	     ChannelVsThreshold->Fill(thresholds[number_of_skips],ch);
	     TimeDiffVsFPGA->Fill(fpga,tdiff);
	   }
	   //x_bin = trb_hit_x_bin[j];
	   //y_bin = trb_hit_y_bin[j];
	   
	   //std::cout << "X Bin: " << x_bin << ", Y Bin: " << y_bin << std::endl;
	   //channel_plot->Fill(trb_hit_y_bin[j],trb_hit_x_bin[j]);
	   
	 }
       
       }
     }
     
   }
   
   for(int i=0; i<1024; i++){
     for(int j=0; j<number_of_skips;j++){
       int iter = gr[i]->GetN();
       gr[i]->SetPoint(iter,thresholds[j],nhits[i][j]/(double)trb3_chain_entries); 
       gr[i]->SetPointError(iter,0,sqrt(nhits[i][j])/(double)trb3_chain_entries); 
       iter = gr_time[i]->GetN();
       //gr_time[i]->SetPoint(iter,thresholds[j],nhits_time[i][j]/(double)trb3_chain_entries); 
       //gr_time[i]->SetPointError(iter,0,sqrt(nhits_time[i][j])/(double)trb3_chain_entries); 
     }
   }
   
   


  TFile *fout = new TFile("thresh_scan_data_one_204.root","RECREATE");
  std::cout << "Writing Graphs....." << std::endl;
  for(int i=0; i<1024; i++) gr[i]->Write(Form("scan_channel_%d",i));
  //for(int i=0; i<500; i++) gr_time[i]->Write(Form("scan_channel_tc_%d",i));
  for(int i=0; i<1024; i++) hist_time[i]->Write();
  
  //channel_plot->Write();
  ChannelVsThreshold->Draw("colz");
  ChannelVsThreshold->Write();
  TimeDiffVsFPGA->Draw("colz");
  TimeDiffVsFPGA->Write();
  fout->Close();

}
   
  

