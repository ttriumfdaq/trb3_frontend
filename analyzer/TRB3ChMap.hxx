//Class to define TRB3. Used for channel mapping/
// Andrew Sikora, Blair Jamieson
// June 12, 2019

#ifndef TRB3_CH_MAP_H
#define TRB3_CH_MAP_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstddef>

//class PMT {
//public: 
//  int get_PMT_Connector(){return PMTConnector;}

//private:
//  int PMTConnector;
//};

// One TDC connector to PMT connector
struct ConPairing{
  int tdcCon;      // Connector on TDC (0-3), labelled (1-4 on TDC)
  int PMTCon;      // Connector on PMT (0-3), labelled (1-4 on Hamamatsu doc)
  int pmtX;        // x location in PMT sized pixels
  int pmtY;        // y location in PMT sized pixels
};


// Each TDC has an ID number, and a collection of pairings
// of connectors on the TDC to connectors on the PMT
class TDC {
public:
  int get_tdc_id(){return tdcid;}
  TDC(int id, std::vector<ConPairing>* pairs): tdcid(id), tdcToPMT(*pairs) {}
  std::vector<ConPairing>* GetPairingVector(){return &tdcToPMT;}

private:
  int tdcid;
  std::vector<ConPairing> tdcToPMT;
};

// Each TRB3 has an ID and collection of TDCs
class TRB3 {
public:
  int get_trb_id(){return trbid;}
  TRB3(int id, std::vector<TDC>* tdcvec): trbid(id), tdc(*tdcvec){}
  std::vector<TDC>* GetTDCVector(){return &tdc;}

private:
  int trbid;
  std::vector<TDC> tdc;
};

// Singleton holding information about current setup
class TRB3Config {
public:
  const int ChannelsPerConnection = 16;
  const int nConnectors = 4;

  // Get a pointer to the only instance of this object
  static TRB3Config * Get( const std::string& filename="" );

  // Get the x coordinate for this trb, tdc and channel
  int GetXCoord(int Evtrbid, int Evtdcid, int EvCh) const;

  // Get the y coordinate for this trb, tdc and channel
  int GetYCoord(int Evtrbid, int Evtdcid, int EvCh) const;

  // Get PMT pixel number, and X,Y location
  int GetPMTCH(int Evtrbid, int Evtdcid, int EvCh, int& X, int& Y) const; 

  // Print all of the connection information in a verbose manner
  void PrintCombinations() const;


private:
  bool verbose = true;

  TRB3Config( const std::string & filename  );

  // Read in configuration file and fill
  void GetTRBConfig(const std::string& filename);

  // Get the PMT channel (hammamatsu 1-64 == our 0-63)
  // for a given pin number on PMT and connector number on PMT
  int CalculatePMTCH(int pin, int PMTConNum) const;

  // Get x, y coordinate and PMT connector number
  // from trb, tdc, and channel
  void GetPMTInformation (int &PMTX, int &PMTY, int &PMTConNum, 
			  int Evtrbid, int Evtdcid, int EvCh) const;


  static TRB3Config * fInstance;
  static std::vector<int> trb3ids;
  static std::vector<TRB3> trbvec;

};


#endif
