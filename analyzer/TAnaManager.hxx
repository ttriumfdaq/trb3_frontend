#ifndef TAnaManager_h
#define TAnaManager_h

// Use this list here to decide which type of equipment to use.

//#define USE_V792
//#define USE_V1190
//#define USE_L2249
//#define USE_AGILENT
//#define USE_V1720
//#define USE_V1720_CORRELATIONS
//#define USE_V1730DPP
//#define USE_V1730RAW
//#define USE_DT724
#define USE_TRB3
//#define USE_CAMACADC

#include "TH2D.h"
#include "TDataContainer.hxx"
#include "TV792Histogram.h"
#include "TV1190Histogram.h"
#include "TTRB3Histogram.hxx"
#include "TReferenceChannelSingleton.hxx"

// Check the TRB options to specify the reference fpga and channel.
bool CheckTRBOption(std::string option);


/// This is an example of how to organize a set of different histograms
/// so that we can access the same information in a display or a batch
/// analyzer.
/// Change the set of ifdef's above to define which equipment to use.
class TAnaManager  {
public:
  TAnaManager();
  virtual ~TAnaManager(){};

  /// Processes the midas event, fills histograms, etc.
  int ProcessMidasEvent(TDataContainer& dataContainer);

  void BeginRun(int transition,int run,int time) {};
  void EndRun(int transition,int run,int time) {};


  /// Methods for determining if we have a particular set of histograms.
  bool HaveTRB3Histograms();

  /// Methods for getting particular set of histograms.
  TTRB3Histograms* GetTRB3Histograms();
  TTRB3FineHistograms* GetTRB3FineHistograms(){return fTRB3FineHistograms;}
  TTRB3DiffHistograms* GetTRB3DiffHistograms();
  PMTDisplay* GetPMTDisplay();
  PMTNhitsDisplay* GetPMTNhitsDisplay();
  TTRB3DiffTrigHistograms* GetTRB3DiffTrigHistograms();
  TRB3Hits* GetTRB3Hits();
  TRB3NHits* GetTRB3NHits();

  TV792Histograms*  GetV792Histograms(){return fV792Histogram;}
  TV1190Histograms*  GetV1190Histograms(){return fV1190Histogram;}

  PMTNhChanDisplay* GetPMTNhChanDisplay(){ return fPMTNhChanDisplay; }

  TH2D *fPMTDisplayHistEvent;
  TH2D *fHitsVsTOF;

private:
  
  TTRB3Histograms *fTRB3Histograms;
  TTRB3FineHistograms *fTRB3FineHistograms;
  TTRB3DiffHistograms *fTRB3DiffHistograms;
  PMTDisplay *fPMTDisplayHist;/////added AS
  PMTNhitsDisplay *fPMTNhitsDisplayHist;/////added AS
  TV792Histograms *fV792Histogram;
  TV1190Histograms *fV1190Histogram;
  TTRB3DiffTrigHistograms *fTRB3DiffTrigHistograms;
  PMTNhChanDisplay * fPMTNhChanDisplay;
  TRB3Hits *fTRB3Hits;
  TRB3NHits *fTRB3NHits;
};



#endif


