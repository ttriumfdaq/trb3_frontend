//Display program for # of hits on each channel
//hope to get 8x8 2d histogram that represents PMT face
//used to visualize cherenkov rings
//could probably put his code in TTRB3Histogram

#include "TTRB3Data.hxx"
#include "TDirectory.h"
#include "TReferenceChannelSingleton.hxx"
#include <string>
#include "THistogramArrayBase.h"

const int NchannelPerFpga = 64; /// what is right value?
const int Nfpga = 4;

/// Reset the histograms for this canvas
PMTDisplay::PMTDisplay(){  

  SetGroupName("FPGA");
  SetChannelName("Channel");
  SetNumberChannelsInGroup(NchannelPerFpga);
  CreateHistograms();
}

void PMTDisplay::CreateHistograms(){
  

  // Otherwise make histograms
  clear();
  
  std::cout << "Create Histos" << std::endl;
  for(int j = 0; j < Nfpga; j++){ // loop over FPGA    
    for(int i = 0; i < NchannelPerFpga; i++){ // loop over channels    
      
      char name[100];
      char title[100];
      sprintf(name,"TRB3_%i_%i",j,i);
      
      // Delete old histograms, if we already have them
      TH1D *old = (TH1D*)gDirectory->Get(name);
      if (old){
        delete old;
      }
      
      
      // Create new histograms
      
      sprintf(title,"64 Channel PMT Face");	
      
      TH2D *tmp = new TH2D(name,title,8.0,0.0,48.5,8.0,0.0,48.5);
      tmp->SetXTitle("x");
      tmp->SetYTitle("y");
      push_back(tmp);
    }
  }

}

/// Update the histograms for this canvas.
void PMTDisplay::UpdateHistograms(TDataContainer& dataContainer){

  TTRB3Data *data = dataContainer.GetEventData<TTRB3Data>("TRB0");
  if(!data) data = dataContainer.GetEventData<TTRB3Data>("TRBA");
  if(data){
    //data->Print();
    
    double reftime = 0;
    for(int i = 0; i < data->GetNumberMeasurements(); i++){
      std::vector<TrbTdcMeas> meas =  data->GetMeasurements();
      double time = meas[i].GetFinalTime();
      uint32_t id = meas[i].GetBoardId();
      uint32_t ch = meas[i].GetChannel();
      int hch = NchannelPerFpga*id + ch;
      //      std::cout << hch << " " << id << " " << ch << std::endl;
      GetHistogram(hch)->Fill(time);

    }

  }    

}

/// Take actions at begin run
void TPMTDisplay::BeginRun(int transition,int run,int time){

  CreateHistograms();

}
