#include "TReferenceChannelSingleton.hxx"


// Allocating and initializing GlobalClass's
// static data member.  
TReferenceChannelSingleton* TReferenceChannelSingleton::s_instance = 0;

TReferenceChannelSingleton::TReferenceChannelSingleton(){
 
  fReferenceChannel = 0;
  fReferenceModule = 0;
};


TReferenceChannelSingleton* TReferenceChannelSingleton::instance()
{
  if (!s_instance)
    s_instance = new TReferenceChannelSingleton();
  return s_instance;
}
