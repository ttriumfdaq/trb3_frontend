//Class to define TRB3. Used for channel mapping/
// Andrew Sikora, Blair Jamieson
// June 12, 2019

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <set>
#include "TRB3ChMap.hxx"

std::vector<int> TRB3Config::trb3ids;
std::vector<TRB3> TRB3Config::trbvec;
TRB3Config * TRB3Config::fInstance = 0;

TRB3Config * TRB3Config::Get( const std::string& filename ) {
  if ( fInstance == 0 ) fInstance = new TRB3Config( filename );
  return fInstance;
}

TRB3Config::TRB3Config(const std::string& filename){
  GetTRBConfig( filename );
}


void TRB3Config::GetTRBConfig(const std::string& filename){
  std::ifstream file;
  file.open(filename.c_str());

  bool first=true;
  int trbid=-1, tdcid=-1, pmtid=-1, pmtcon=-1, tdcCon=-1, pmtx=-1, pmty=-1;
  int prevtrbid=-1, prevtdcid=-1, prevpmtid=-1, prevpmtcon=-1, prevtdcCon=-1, prevglobX=-1, prevglobY=-1;
  std::vector<int> tdcids;
  std::vector<int> pmtids;
  std::vector<int> pmtcons;
  std::vector<int> tdcCons;
  std::vector<int> globXs;
  std::vector<int> globYs;

  std::streampos pos;
  
  std::string line;

  while (getline(file,line)){    
    std::istringstream is(line);
    
    if(first){
      first = false;
      pos = file.tellg();
      continue;
    }   
    std::vector<TDC> tdcvec;    
    file.seekg(pos);
    while(getline(file, line)){
      
      std::istringstream iss(line);
      iss>>trbid>>tdcid>>pmtid>>pmtcon>>tdcCon>>pmtx>>pmty;     
      
      if(trbid != prevtrbid){       
	prevtrbid = trbid;
	file.seekg(pos);
	break;
      }
      
      //TDC TDCtemp;
      std::vector<ConPairing> PairsTemp;

      file.seekg(pos);
      while (getline(file,line)){     
	std::istringstream isss(line);
	isss>>trbid>>tdcid>>pmtid>>pmtcon>>tdcCon>>pmtx>>pmty;      
	if (tdcid != prevtdcid){
	  file.seekg(pos);
	  break;
	}	
	ConPairing temp;
	temp.tdcCon = tdcCon;
	temp.PMTCon = pmtcon;
	temp.pmtX = pmtx;
	temp.pmtY = pmty;
	PairsTemp.push_back(temp);
	prevtdcid = tdcid;	
	pos = file.tellg();
      }
      TDC TDCtemp(prevtdcid, &PairsTemp);
      prevtdcid = tdcid;
      
      tdcvec.push_back(TDCtemp);
      prevtrbid = trbid;

      pos = file.tellg();      
    }
    TRB3 TRBtemp(trbid, &tdcvec);
    trbvec.push_back(TRBtemp);    
  }
  if (verbose)
    std::cout<<"Filled TRB3 vector"<<std::endl;  
  for (int k =0; k < trbvec.size(); ++k){
    std::vector<TDC>* tdcs = trbvec[k].GetTDCVector();  
    for (int i =0; i < tdcs->size(); ++i){	      
      std::vector<ConPairing>* pairs = (*tdcs)[i].GetPairingVector();	        
      for (int j=0; j<pairs->size(); ++j){	
	if (verbose)
	  std::cout<<"TRBId: "<<trbvec[k].get_trb_id()<<" \tTDCId: "
		 <<(*tdcs)[i].get_tdc_id()<<"\tTDCCon: "
		 <<(*pairs)[j].tdcCon<<"\tPMTCon: "
		 <<(*pairs)[j].PMTCon<<std::endl;
      } 
    }  
  }
}



int TRB3Config::GetPMTCH( int Evtrbid, int Evtdcid, int EvCh, int& PMTX, int& PMTY) const {

  //trb3 data gives channels from 1-64. 
  //channel mapping file uses channels 0-63.
  //Therefore subtract 1 from trb3 channel to match
  --EvCh;

  //Determine which "pin" on the PMTConnector the signal came from
  int pin = EvCh%ChannelsPerConnection;
  int PMTConNum=0;
  PMTX=0;
  PMTY=0;

  //Call funtion to determine PMTConnector
  GetPMTInformation(PMTX, PMTY, PMTConNum, Evtrbid, Evtdcid, EvCh);

  //Using pin and PMTConnector information determine which PMT channel
  //signal came from.
  int PMTCH;
  PMTCH = CalculatePMTCH(pin, PMTConNum );

  return PMTCH;
}



int TRB3Config::GetXCoord( int Evtrbid, int Evtdcid, int EvCh) const {
  int PMTCH;
  int PMTX, PMTY;
  PMTCH = GetPMTCH( Evtrbid, Evtdcid, EvCh, PMTX, PMTY );

  return ( (7-PMTCH%8) + 8*PMTX);
}



int TRB3Config::GetYCoord( int Evtrbid, int Evtdcid, int EvCh) const {
  int PMTCH;
  int PMTX, PMTY;
  PMTCH = GetPMTCH( Evtrbid, Evtdcid, EvCh, PMTX, PMTY );
  //std::cout<<"YPMTCH: "<<PMTCH<<std::endl;

  return ( (PMTCH/8) + 8*PMTY);

}


//Prints all possible fpga/TDCConnection/PMTConnection combinations to check that mapping is correct
void TRB3Config::PrintCombinations() const{

  if(0){
    for(int Con =0; Con<4;++Con){
      for(int pin =0; pin<16; ++pin){
	std::cout<<"Con: "<<Con<<"\tpin: "
		 <<pin<<"\tPMTCH: "
		 <<CalculatePMTCH(pin, Con)<<std::endl;
      }
    }
  }

  std::set<int> pmtchannels;

  int PMTConNum = 0;
  int PMTX=0;
  int PMTY=0;

  for(int tdcid=1; tdcid <3; ++tdcid){
    if(tdcid == 1){
      for(int EvCh = 0; EvCh<48;++EvCh){
	int EvtdcCon = EvCh/ChannelsPerConnection;
	GetPMTInformation(PMTX, PMTY, PMTConNum, 0, tdcid, EvCh);
	int pin = EvCh%ChannelsPerConnection;
	int PMTCH = CalculatePMTCH(pin, PMTConNum);
	std::cout<<"tdcid: "<<tdcid<<"\tEvCh: "<<EvCh
		 <<"\tPMTConNum: "<<PMTConNum
		 <<"\tEvtdcCon: "<<EvtdcCon<<"\tPMTX: "
		 <<PMTX<<"\tPMTY: "<<PMTY
		 <<"\tPMTCH: "<<PMTCH
		 <<"\t(x,y)=("<<(PMTCH%8 + 8*PMTX)
		 <<", "<< (PMTCH/8 + 8*PMTY) <<" )"
		 <<std::endl;
	pmtchannels.insert( PMTCH );
      }
    }
    if(tdcid == 2){
      for(int EvCh = 0; EvCh<16;++EvCh){
	int EvtdcCon = EvCh/ChannelsPerConnection;
	GetPMTInformation(PMTX, PMTY, PMTConNum, 0, tdcid, EvCh);
	int pin = EvCh%ChannelsPerConnection;
	int PMTCH = CalculatePMTCH(pin, PMTConNum);
	std::cout<<"tdcid: "<<tdcid<<"\tEvCh: "<<EvCh
		 <<"\tPMTConNum: "<<PMTConNum
		 <<"\tEvtdcCon: "<<EvtdcCon<<"\tPMTX: "
		 <<PMTX<<"\tPMTY: "<<PMTY
		 <<"\tPMTCH: "<<PMTCH
		 <<"\t(x,y)=("<<(PMTCH%8 + 8*PMTX)
		 <<", "<< (PMTCH/8 + 8*PMTY) <<" )"
		 <<std::endl;
	pmtchannels.insert( PMTCH );
      }
    }
  }

  std::cout<<"Number of pmt channels matched = "<<pmtchannels.size()<<std::endl;
  for ( std::set<int>::const_iterator it=pmtchannels.begin(); it!=pmtchannels.end(); ++it ){
    std::cout << *it <<" ";
  }
  std::cout<<std::endl;

}

//Calculates PMT channel from pin # and PMTConnection #
int TRB3Config::CalculatePMTCH(int pin, int PMTConNum) const {
  int PMTCH;

  
  PMTCH = ((int)(pin/2))*8 + pin%2 + PMTConNum*2;
  return PMTCH;
}


//    Searches through trb3 vector to find matching trbid, then searches tdc vector to find matching tdcid, 
//    the finally finds the corresponding tdc connector. Used tdc connector information to determine which 
//    PMT connection the signal came from.
void TRB3Config::GetPMTInformation (int &PMTX, int &PMTY, int &PMTConNum, int Evtrbid, int Evtdcid, int EvCh) const {


  int EvtdcCon = EvCh/ChannelsPerConnection;
  bool matched = false;

  for (int it1 = 0; it1 < trbvec.size(); ++it1){

    if(trbvec[it1].get_trb_id() == Evtrbid){
      std::vector<TDC>* tdcs = trbvec[it1].GetTDCVector();
      
      for (int i =0; i < tdcs->size(); ++i){	  
	if ((*tdcs)[i].get_tdc_id() == Evtdcid){
	  std::vector<ConPairing>* pairs = (*tdcs)[i].GetPairingVector();	  
	  
	  for (int j=0; j<pairs->size(); ++j){
	    if ((*pairs)[j].tdcCon == EvtdcCon){
	      PMTConNum = (*pairs)[j].PMTCon;
	      PMTX = (*pairs)[j].pmtX;
	      PMTY = (*pairs)[j].pmtY;
	      matched = true;
	      break;
	    }
	    if(matched) break;
	  }
	}
	if(matched) break;
      }
    }
    if(matched) break;
  }

}




