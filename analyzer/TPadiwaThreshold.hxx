#ifndef TPadiwaThreshold_hxx_seen
#define TPadiwaThreshold_hxx_seen

#include <vector>

#include "TGenericData.hxx"



/// Class for storing data from Padiwa Thresholds
class TPadiwaThreshold: public TGenericData {

 
public:

  /// Constructor
  TPadiwaThreshold(int bklen, int bktype, const char* name, void *pdata);

  // Get the threshold for this asic, channel
  int GetThreshold(int asic, int channel){

    // Error checking
    if(GetSize() > 256) return -1;
    if(asic > 4) return -1;
    if(channel > 64) return -1;

    int index = asic * 64 + channel;
    return GetData32()[index];
			  

  } 
  
  // Get threshold for this TRB3 channel.
  int GetThreshold(int chan){
    if(chan < 0 || chan > 256) return -1;
    
    return GetData32()[chan];
    
  }


private:

  /// Vector of thresholds.
  std::vector<int> fThresholds;



};

#endif
