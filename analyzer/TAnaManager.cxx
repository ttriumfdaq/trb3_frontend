#include "TAnaManager.hxx"
#include "TV1720RawData.h"
#include "TRB3Decoder.hxx"
#include "TRB3ChMap.hxx"
#include "TTRB3DataV2.hxx"

// Check the TRB options to specify the reference fpga and channel.
bool CheckTRBOption(std::string option){
  
  const char* arg = option.c_str();
  if (strncmp(arg,"-trb_ref_fpga=",14)==0){  // Event cutoff flag (only applicable in offline mode)      
    int ref_fpga = atoi(arg+14);
    if(ref_fpga >=0 && ref_fpga <= 3){
      printf("Setting TRB3 reference FPGA to %i\n",ref_fpga);        
      TReferenceChannelSingleton::instance()->SetReferenceModule(ref_fpga);
    }else{
      printf("Invalid FPGA number %i\n",ref_fpga);
      }
    return true;
  }
  
  if (strncmp(arg,"-trb_ref_chan=",14)==0){  // Event cutoff flag (only applicable in offline mode)      
    int ref_chan = atoi(arg+14);
    if(ref_chan >=0 && ref_chan <= 64){
      printf("Setting TRB3 reference Channel to %i\n",ref_chan);        
      TReferenceChannelSingleton::instance()->SetReferenceChannel(ref_chan);
    }else{
      printf("Invalid FPGA channel %i\n",ref_chan);
    }
    return true;
  }
  return false;
}

TAnaManager::TAnaManager(){


    fTRB3Histograms = 0;
    fTRB3FineHistograms = 0;
    fTRB3DiffHistograms = 0;
	fPMTDisplayHist = 0;
	fPMTNhitsDisplayHist = 0;
	fTRB3Hits = 0;
	//fTRB3NHits = 0;
#ifdef USE_TRB3

        // Set the TDC linear calibration values
        Trb3Calib::getInstance().SetTRB3LinearCalibrationConstants(17,471);
        
    fTRB3Histograms = new TTRB3Histograms();
		fTRB3Histograms->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
	fTRB3FineHistograms = new TTRB3FineHistograms();
		fTRB3FineHistograms->DisableAutoUpdate();  
    fTRB3DiffHistograms = new TTRB3DiffHistograms();
        fTRB3DiffHistograms->DisableAutoUpdate();
	fPMTDisplayHist = new PMTDisplay();
		fPMTDisplayHist->DisableAutoUpdate();
	fTRB3Hits = new TRB3Hits();
		fTRB3Hits->DisableAutoUpdate();
	fTRB3NHits = new TRB3NHits();
		fTRB3NHits->DisableAutoUpdate();

	fPMTDisplayHistEvent = new TH2D("fPMTDisplayHistEvent","PMT Display Event",20,-9.5,10.5,20,-9.5,10.5);
	fHitsVsTOF = new TH2D("HitsVsTOF","Hits vs TOF",500,-50,50,75,-4.5,70.5);
	fHitsVsTOF->SetXTitle("Time difference: S3 - S1");
	fHitsVsTOF->SetYTitle("Number of ARICH hits");


	fPMTNhitsDisplayHist = new PMTNhitsDisplay();
		fPMTNhitsDisplayHist->DisableAutoUpdate();
    fTRB3DiffTrigHistograms = new TTRB3DiffTrigHistograms();
        fTRB3DiffTrigHistograms->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.

	fPMTNhChanDisplay = new PMTNhChanDisplay();
		fPMTNhChanDisplay->DisableAutoUpdate();
#endif

	fV792Histogram = 0;
	fV792Histogram = new TV792Histograms();
		fV792Histogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.

	fV1190Histogram = 0;
	fV1190Histogram = new TV1190Histograms();
		fV1190Histogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.



};



int TAnaManager::ProcessMidasEvent(TDataContainer& dataContainer){

        fV792Histogram->UpdateHistograms(dataContainer); 
        fV1190Histogram->UpdateHistograms(dataContainer); 

        if(fTRB3Histograms)  fTRB3Histograms->UpdateHistograms(dataContainer);
        if(fTRB3FineHistograms)  fTRB3FineHistograms->UpdateHistograms(dataContainer);
        if(fTRB3DiffHistograms)  fTRB3DiffHistograms->UpdateHistograms(dataContainer);
		if(fTRB3Hits) fTRB3Hits->UpdateHistograms(dataContainer);
		if(fTRB3NHits) fTRB3NHits->UpdateHistograms(dataContainer);
		if(fPMTDisplayHist) fPMTDisplayHist->UpdateHistograms(dataContainer);
		if(fPMTNhitsDisplayHist) fPMTNhitsDisplayHist->UpdateHistograms(dataContainer);
		if(fPMTNhChanDisplay) fPMTNhChanDisplay->UpdateHistograms(dataContainer);
        if(fTRB3FineHistograms)  fTRB3FineHistograms->UpdateHistograms(dataContainer); 
        if(fTRB3DiffHistograms)  fTRB3DiffHistograms->UpdateHistograms(dataContainer); 
        if(fTRB3DiffTrigHistograms)  fTRB3DiffTrigHistograms->UpdateHistograms(dataContainer); 

	//TTRB3DataV2 *data = dataContainer.GetEventData<TTRB3DataV2>("TRB0");   // Use for trb000
	TTRB3DataV2 *data = dataContainer.GetEventData<TTRB3DataV2>("TRB1");  // Use for trb001
	if(data){

	  
	  // calculate total hits 
	  // calculate tof
	  int total_hits = 0;
	  double s1_time = 0, s2_time = 0, s3_time = 0;
	  double ref_time_fpga1 = 0;
	  for(int i = 0; i < data->GetNumberMeasurements(); i++){
	    std::vector<TrbTdcMeasV2> meas =  data->GetMeasurements();
	    double time = meas[i].GetSemiFinalTime();
	    uint32_t id = meas[i].GetBoardId();
	    uint32_t ch = meas[i].GetChannel();
	    //if((id == 1 or id == 2) and ch > 0)
	    if( (id==1 && ch>0 && ch<49) || (id==2 && ch>0 && ch<17) )
	      total_hits++;

	    if(id == 0 and ch == 1) s1_time = time;
	    if(id == 0 and ch == 3) s2_time = time;
	    if(id == 0 and ch == 5) s3_time = time;
	    if(id == 1 and ch == 0) ref_time_fpga1 = time;

	  }
	  
	  double tof = (s3_time - s1_time) * 0.001;

	  //std::cout << "Total : " << total_hits << std::endl;

	  if(total_hits > 5){

	    fPMTDisplayHistEvent->Reset();
	    TRB3Config * chmap = TRB3Config::Get();

            /*for(int i=1; i<49; i++){
              int xbin = chmap->GetXCoord(0, 1, i);
              int ybin = chmap->GetYCoord(0, 1, i); 
              std::cout << "1," << i << ":  " << xbin << "," << ybin << std::endl;
              if(i<17){
                xbin = chmap->GetXCoord(0, 2, i);
                ybin = chmap->GetYCoord(0, 2, i); 
                std::cout << "2," << i << ":  " << xbin << "," << ybin << std::endl;
              }
            } */  
                 


	    for(int i = 0; i < data->GetNumberMeasurements(); i++){
	      std::vector<TrbTdcMeasV2> meas =  data->GetMeasurements();
	      double time = meas[i].GetFinalTime();
	      uint32_t fpga = meas[i].GetBoardId();
	      uint32_t ch = meas[i].GetChannel();
	      //if(!((fpga == 1 or fpga == 2) and ch > 0)) continue;
	      if( !((fpga==1 && ch>0 && ch<49) || (fpga==2 && ch>0 && ch<17))) continue;

	      double time_diff = time - ref_time_fpga1;
	      //std::cout << time_diff << std::endl;
	      if(time_diff < -335000 || time_diff > -310000) continue;

	      
	      int xbin = chmap->GetXCoord(0, fpga, ch);
	      int ybin = chmap->GetYCoord(0, fpga, ch);
              //if(xbin==1 && ybin==7) std::cout << "Channel for 1,7: " << fpga << " " << ch << std::endl;
	      
	      fPMTDisplayHistEvent->Fill( float(xbin-4), float(ybin-4) );
	    }
	  }    

	  //std::cout << tof << " " << total_hits << std::endl;
	  fHitsVsTOF->Fill(tof,total_hits);

	  
	}

       return 1;
}


bool TAnaManager::HaveTRB3Histograms(){
	if(fTRB3Histograms) return true; 
	return false;
};

TTRB3Histograms* TAnaManager::GetTRB3Histograms(){return fTRB3Histograms;}
TTRB3DiffHistograms* TAnaManager::GetTRB3DiffHistograms(){return fTRB3DiffHistograms;}
TRB3Hits* TAnaManager::GetTRB3Hits(){return fTRB3Hits;}
TRB3NHits* TAnaManager::GetTRB3NHits(){return fTRB3NHits;}
PMTDisplay* TAnaManager::GetPMTDisplay(){return fPMTDisplayHist;}
PMTNhitsDisplay* TAnaManager::GetPMTNhitsDisplay(){return fPMTNhitsDisplayHist;}
TTRB3DiffTrigHistograms* TAnaManager::GetTRB3DiffTrigHistograms(){return fTRB3DiffTrigHistograms;}

