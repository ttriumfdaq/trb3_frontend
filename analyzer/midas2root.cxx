// Example Program for converting MIDAS format to ROOT format.

// This converter is for TRB3 midas->root

#include <stdio.h>
#include <iostream>
#include <time.h>
#include <vector>

#include "TRootanaEventLoop.hxx"
#include "TFile.h"
#include "TTree.h"

#include "TAnaManager.hxx"
#include "TRB3DecoderV2.hxx"
#include "TRB3ChMap.hxx"
#include "TTRB3DataV2.hxx"
#include "TPadiwaThreshold.hxx"

//#ifdef USE_V792
//#include "TV792Data.hxx"
//#endif

class Analyzer: public TRootanaEventLoop {

public:

  // An analysis manager.  Define and fill histograms in 
  // analysis manager.
  TAnaManager *anaManager;

  // The tree to fill.
  TTree *fTree;
  TTree *fTree2;


  int timestamp;
  int serialnumber;
  
#ifdef USE_V792
  // CAEN V792 tree variables
  int nchannels;
  int adc_value[32];
#endif

  

  int counter; 
  int trb_hits;
  int thresh_hits;
  int timestamp_count;
  int number_threshold_change;
  int final_fpga;
  int true_ch;
  int thresh_ch;
  double trb_hit_final_time[5000];
  double trb_hit_time[5000];
  double trb_hit_epoch[5000];
  int trb_hit_trb_num[5000];
  int trb_hit_fpga[5000];
  int trb_hit_ch[5000];
  int trb_hit_mapmt[5000];
  int trb_hit_mapmt_ch[5000];
  int trb_hit_x_bin[5000];
  int trb_hit_y_bin[5000];
  int padiwa_threshold[1024];
  int threshold_previous[1024];
  double threshold_time[1024];

  double fLastTRBHitTime; // time of last TRB hit.

  Analyzer() {
    UseBatchMode();
  };

  virtual ~Analyzer() {};

  void Initialize(){


  }
  
  
  void BeginRun(int transition,int run,int time){
   
    // Create a TTree
    fTree = new TTree("midas_data","MIDAS data");
    fTree2 = new TTree("threshold_times","threshold_times");
   

    fTree->Branch("timestamp",&timestamp,"timestamp/I");
    fTree->Branch("serialnumber",&serialnumber,"serialnumber/I");

#ifdef USE_V792    
    fTree->Branch("nchannels",&nchannels,"nchannels/I");
    fTree->Branch("adc_value",adc_value,"adc_value[nchannels]/I");
#endif
   
    
    fTree->Branch("trb_hits",&trb_hits,"trb_hits/I");
    fTree->Branch("trb_hit_final_time",trb_hit_final_time,"trb_hit_final_time[trb_hits]/D");
    fTree->Branch("trb_hit_epoch",trb_hit_epoch,"trb_hit_epoch[trb_hits]/D");
    fTree->Branch("trb_hit_trb_num",trb_hit_trb_num,"trb_hit_trb_num[trb_hits]/I");
    fTree->Branch("trb_hit_time",trb_hit_time,"trb_hit_time[trb_hits]/D");
    fTree->Branch("trb_hit_fpga",trb_hit_fpga,"trb_hit_fpga[trb_hits]/I");
    fTree->Branch("trb_hit_ch",trb_hit_ch,"trb_hit_ch[trb_hits]/I");
    fTree->Branch("trb_hit_mapmt",trb_hit_mapmt,"trb_hit_mapmt[trb_hits]/I");
    fTree->Branch("trb_hit_mapmt_ch",trb_hit_mapmt_ch,"trb_hit_mapmt_ch[trb_hits]/I");
    fTree->Branch("trb_hit_x_bin",trb_hit_x_bin,"trb_hit_x_bin[trb_hits]/I");
    fTree->Branch("trb_hit_y_bin",trb_hit_y_bin,"trb_hit_y_bin[trb_hits]/I");
  
    fTree2->Branch("trb_hits",&trb_hits,"trb_hits/I");
    fTree2->Branch("thresh_hits",&thresh_hits,"thresh_hits/I");
    fTree2->Branch("padiwa_threshold",padiwa_threshold,"padiwa_threshold[1024]/I");    
    fTree2->Branch("threshold_time",threshold_time,"threshold_time[1024]/D");


 


    for(int i = 0; i < 1024; i++) padiwa_threshold[i] = 0;
    
  
  }   

  

  void EndRun(int transition,int run,int time){
        printf("\n");
  }

  
  
  // Main work here; create ttree events for every sequenced event in 
  // Lecroy data packets.
  bool ProcessMidasEvent(TDataContainer& dataContainer){

    serialnumber = dataContainer.GetMidasEvent().GetSerialNumber();
    //if(serialnumber%10 == 0) printf(".");
    timestamp = dataContainer.GetMidasEvent().GetTimeStamp();
 
#ifdef USE_V792    
    TV792Data *data = dataContainer.GetEventData<TV792Data>("ADC0");
    if(data){
      nchannels = 32;
      for(int i = 0; i < nchannels;i++) adc_value[i] = 0;
      
      /// Get the Vector of ADC Measurements.
      std::vector<VADCMeasurement> measurements = data->GetMeasurements();
      for(unsigned int i = 0; i < measurements.size(); i++){ // loop over measurements
        
        int chan = measurements[i].GetChannel();
        uint32_t adc = measurements[i].GetMeasurement();
        
        if(chan >= 0 && chan < nchannels)
          adc_value[chan] = adc;
      }
    }
#endif

    // Fill TRB data:

    trb_hits = 0;
    thresh_hits = 0;
    number_threshold_change = 0;
    timestamp_count = 0;
    counter = 0;
    


    //Loop introduced here to iterate over all TRB3 data containers (TRB0,TRB1 etc.)
    
    for (int k = 0; k < 4; k++){ // update as we increase number of TRB3s in the set-up
      char bankname[5];
      sprintf(bankname,"TRB%i",k);
      //std::cout << bankname << std::endl;
      trb_hits = 0;
     
      for(int i = 0; i < 5000; i++){

	trb_hit_epoch[i] = 0;
	trb_hit_time[i] = 0;
	trb_hit_final_time[i] = 0;
	trb_hit_trb_num[i] = 0;
	trb_hit_fpga[i] = 0;
	trb_hit_ch[i]   = 0;
	trb_hit_mapmt[i] = 0;
	trb_hit_mapmt_ch[i]   = 0;
	trb_hit_x_bin[i] = 0;
	trb_hit_y_bin[i] = 0;
      }
      
      

      TTRB3DataV2 *data = dataContainer.GetEventData<TTRB3DataV2>(bankname);
      if(!data) data = dataContainer.GetEventData<TTRB3DataV2>("TRBA");
      if(data){
      
	int timestamp = dataContainer.GetMidasEvent().GetTimeStamp();
	//TRB3Config * chmap = TRB3Config::Get();
	
	std::vector<TrbTdcMeasV2> meas =  data->GetMeasurements();
	for(int i = 0; i < data->GetNumberMeasurements(); i++){

	  int fpga = meas[i].GetBoardId();
	  int ch = meas[i].GetChannel();
	  //std::cout << fpga << " " << ch << " " << meas[i].GetFinalTime() << std::endl;

	  final_fpga = (4*k) + fpga; // Allows us to distinguish between different TRB3 databanks. TRB3-0 = FPGA 0-3, TRB3-1 = FPGA 4-7 etc.
	  if (ch != 0){
	    true_ch = (64*(final_fpga)) + ch;
	  }else{
	    true_ch = 0; // This is done to avoid any overlap between channel labelling - the analsysis code only reads channel 0 for reference times, where we also have a matching FPGA number, so this doesn't cause any problems reading out the data.
	  }
	 
	  //std::cout << "FPGA: " << final_fpga << "Channel: " << true_ch << std::endl;
	  //std::cout << "i: " << i << std::endl;
	  //std::cout << "Saved hit!" << std::endl;

	  trb_hit_time[trb_hits] = meas[i].GetSemiFinalTime();
	  fLastTRBHitTime = meas[i].GetFinalTime();
	  trb_hit_epoch[trb_hits] = meas[i].GetEpochCounter();
	  trb_hit_final_time[trb_hits] = meas[i].GetFinalTime();
	      
	      
	  trb_hit_fpga[trb_hits] = final_fpga;
	  trb_hit_ch[trb_hits] = true_ch;
	  //std::cout << "trb_hit_fpga[trb_hits]: " << trb_hit_fpga[trb_hits] << std::endl;
	  //std::cout << "trb_hit_ch[trb_hits]: " <<  trb_hit_ch[trb_hits] << std::endl;
	  trb_hit_trb_num[k]++; 
	  
	  //std::cout << "What channel? " << trb_hit_ch[trb_hits] << std::endl;
	  //trb_hit_x_bin[trb_hits] = chmap->GetXCoord(0, trb_hit_fpga[i] , trb_hit_ch[i] );
	  //trb_hit_y_bin[trb_hits] = chmap->GetYCoord(0, trb_hit_fpga[i] , trb_hit_ch[i] );
	  trb_hits++;
	      
	}
	fTree->Fill();
      }
    }
    //Loop for reading Padiwa thresholds and the resulting times

    for (int k = 0; k < 4; k++){
      char bankname[5];
      sprintf(bankname,"PAD%i",k);
      TPadiwaThreshold *data2 = dataContainer.GetEventData<TPadiwaThreshold>(bankname);
      thresh_hits = 0;
      for(int i = 0; i < 1024; i++){
	threshold_time[i] = 0;
      }
      if(data2){
	//std::cout << "fLastTRBHitTime: " << fLastTRBHitTime << std::endl;	
	//std::cout << "\nThresholds for first channel of each ASIC: " << std::endl;
	  // << data2->GetThreshold(0)  << " " 
	  //	  << data2->GetThreshold(64) << " " 
	  //	  << data2->GetThreshold(128) << " " 
	  //	  << data2->GetThreshold(192) <<std::endl;
	  
	for(int ch = 0; ch < 256; ch++){
	  
	  thresh_ch = (256*k) + ch;
	  padiwa_threshold[thresh_ch] = data2->GetThreshold(ch);
	  threshold_time[thresh_ch] = fLastTRBHitTime;
	  //if (thresh_ch%64 == 0){
	  //  std::cout << "Channel " << thresh_ch << ", Threshold: " <<  data2->GetThreshold(ch) << std::endl;
	  //}
	}
	thresh_hits++;
        
	fTree2->Fill();
    
     
      }
    }
    
    return true;
      
	
  };
  
  // Complicated method to set correct filename when dealing with subruns.
  std::string SetFullOutputFileName(int run, std::string midasFilename)
  {
    char buff[128]; 
    Int_t in_num = 0, part = 0;
    Int_t num[2] = { 0, 0 }; // run and subrun values
    // get run/subrun numbers from file name
    for (int i=0; ; ++i) {
      char ch = midasFilename[i];
        if (!ch) break;
        if (ch == '/') {
          // skip numbers in the directory name
          num[0] = num[1] = in_num = part = 0;
        } else if (ch >= '0' && ch <= '9' && part < 2) {
          num[part] = num[part] * 10 + (ch - '0');
          in_num = 1;
        } else if (in_num) {
          in_num = 0;
          ++part;
        }
    }
    if (part == 2) {
      if (run != num[0]) {
        std::cerr << "File name run number (" << num[0]
                  << ") disagrees with MIDAS run (" << run << ")" << std::endl;
        exit(1);
      }
      sprintf(buff,"output_%.6d_%.4d.root", run, num[1]);
      printf("Using filename %s\n",buff);
    } else {
      sprintf(buff,"output_%.6d.root", run);
    }
    return std::string(buff);
  };





}; 


int main(int argc, char *argv[])
{

  Analyzer::CreateSingleton<Analyzer>();
  return Analyzer::Get().ExecuteLoop(argc, argv);

}

