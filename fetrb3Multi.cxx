// 
// Modified version of feudp.cxx from Konstantin; 
// repurposed to setup and readout UDP socket from TRB3 FGPA-TDC
// 
// Redo so that we do multi-threading to do the socket readout from multiple TRBs
// Also periodically reads TRB3 temperatures and Padiwa thresholds.



#include <stdio.h>
#include <iostream>
#include <netdb.h> // getnameinfo()
#include <string.h> // memcpy()
#include <errno.h> // errno
#include <string>
#include <vector>
#include <bits/stdc++.h> 
#include <stdlib.h>
#include <sstream>
#include <pthread.h>

//#include <atomic>
#include <thread>
#include <sys/time.h>
#include <sched.h>
#include <sys/resource.h>
#include <fstream>

#include "midas.h"

// Header for TRB3 control
#include <trbnet.h>
#include <trberror.h>

//extern int trb_errno;
// buffer for TRB3 commands
static size_t BUFFER_SIZE = 4194304;  /* 4MByte */
static uint32_t buffer[4194304];    

#include "TUDPSocketHandler.hxx"
 
// Number of TRBs being readout
static const int MAXTRB=5;
static int NTRBs = 5;
bool runInProgress = false; //!< run is in progress
int numberBadTriggerMatch = 0;

std::vector<TUDPSocketHandler> udp_sockets;

const char *frontend_name = "fetrb3UDP";                     /* fe MIDAS client name */
const char *frontend_file_name = __FILE__;               /* The frontend file name */

#ifndef NEED_NO_EXTERN_C
extern "C" {
#endif
   BOOL frontend_call_loop = TRUE;       /* frontend_loop called periodically TRUE */
   int display_period = 0;               /* status page displayed with this freq[ms] */
   int max_event_size = 1*1024*1024;     /* max event size produced by this frontend */
   int max_event_size_frag = 5 * 1024 * 1024;     /* max for fragmented events */
   int event_buffer_size = 8*1024*1024;           /* buffer size to hold events */

  int interrupt_configure(INT cmd, INT source, PTYPE adr);
  INT poll_event(INT source, INT count, BOOL test);
  int frontend_init();
  int frontend_exit();
  int begin_of_run(int run, char *err);
  int end_of_run(int run, char *err);
  int pause_run(int run, char *err);
  int resume_run(int run, char *err);
  int frontend_loop();
  int merge_trb_fragments(char *pevent, INT off);
  INT read_trb3_temperature(char *pevent, INT off);
  INT read_padiwa(char *pevent, INT off);
#ifndef NEED_NO_EXTERN_C
}
#endif

#ifndef EQ_NAME
#define EQ_NAME "TRB3"
#endif

#ifndef EQ_EVID
#define EQ_EVID 1
#endif

EQUIPMENT equipment[] = {
   { EQ_NAME,                         /* equipment name */
      {EQ_EVID, 0, "SYSTEM",          /* event ID, trigger mask, Evbuf */
       EQ_MULTITHREAD, 0, "MIDAS",    /* equipment type, EventSource, format */
       TRUE, RO_ALWAYS,               /* enabled?, WhenRead? */
       50, 0, 0, 0,                   /* poll[ms], Evt Lim, SubEvtLim, LogHist */
       "", "", "",}, merge_trb_fragments,      /* readout routine */
   },
   {"TRB3_TEMP",               /* equipment name */
    {2, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC,              /* equipment type */
     LAM_SOURCE(0, 0xFFFFFF),        /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_ODB|RO_ALWAYS,           /* read only when running */
     2000,                    /* poll for 1000ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     1,                      
     "", "", "",},
    read_trb3_temperature,      /* readout routine */
    },
   {"TRB3_PADIWA",               /* equipment name */
    {2, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC,              /* equipment type */
     LAM_SOURCE(0, 0xFFFFFF),        /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_ODB|RO_ALWAYS,           /* read only when running */
     2000,                    /* poll for 1000ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     1,                      
     "", "", "",},
    read_padiwa,      /* readout routine */
    },
   {""}
};
////////////////////////////////////////////////////////////////////////////

#include <sys/time.h>


static HNDLE hDB;



int interrupt_configure(INT cmd, INT source, PTYPE adr)
{
   return SUCCESS;
}


// This is the list of FPGA IDs for our TRB3
int fpga_ids[4] = {0x100,0x101,0x102,0x103};

// Keep list of all FPGA IDs
std::vector<int> fpga_all_ids;

// Keep list of the FPGA peripheral IDs
std::vector<std::vector<int> > fpga_peri_ids;



// enable/disable block of channels. setclr = true means enable the channel. 
int setEnableBlock(int board, int channel, bool setclr){
  
  int registr = (int)(channel/2) + 0xc802;
  int regvalue;
  if(channel%2)
    regvalue = 0xffff0000;
  else
    regvalue = 0xffff;

  int status;
  if(setclr)
    status = trb_register_setbit(board,registr,regvalue);
  else
    status = trb_register_clearbit(board,registr,regvalue);

  
  if(status == -1)   cm_msg(MERROR,"setup_trb","Problem with setting enable bit ");
  if (trb_errno == TRB_STATUS_WARNING) cm_msg(MERROR,"setup_trb","Status-Bit(s) have been set:\n%s\n", trb_termstr(trb_term));

  return 0;
}
#define MAX_UDP_SIZE (0x10000)

INT setup_trb(){

   std::string path;
   path += std::string("/Equipment/") + EQ_NAME + std::string("/Settings");

   std::string path1 = path + "/UseWindowing";

   BOOL use_windowing = true;
   int size = sizeof(use_windowing);
   int status = db_get_value(hDB, 0, path1.c_str(), &use_windowing, &size, TID_BOOL, TRUE);
 
  // enable/disable the event window (get TDC for some/all hits)
  if(use_windowing){
     cm_msg(MINFO,"setup_trb","Using TDC windowing ");

     std::string path2 = path + "/WindowSize";     
     float window_size = 500;
     size = sizeof(window_size);
     status = db_get_value(hDB, 0, path2.c_str(), &window_size, &size, TID_FLOAT, TRUE);
     printf("Status %i\n",status);
     uint32_t window_size_u;
     if(window_size > 5000){
       cm_msg(MERROR,"setup_trb","ERROR, maximum window size is 5000ns; setting to default of 500ns");
       window_size_u = 0x64;
     }else{
       window_size_u = (uint32_t)(window_size/5.0);
     }

     for(unsigned int j = 0; j < fpga_peri_ids.size(); j++){ // loop over TRBs                                                                                                 
       for(int i = 0; i < 4; i++){// loop over FPGA                                                                                                                            
	 uint32_t register_value = 0x80000000 + ((window_size_u) << 16) + window_size_u;
	 printf("Setting 0xc801 to 0x%x\n",register_value);
	 status = trb_register_write(fpga_peri_ids[j][i],0xc801,register_value);
	 if(status == -1){
	   cm_msg(MERROR,"setup_trb","Failed to set TRB3 TDC registers; lost communication; TRB3 crashed (clock loss?)");
	   return -1;
	 }
       } 
     }
  }else{
     cm_msg(MINFO,"setup_trb","Not using TDC windowing ");
     for(unsigned int j = 0; j < fpga_peri_ids.size(); j++){ // loop over TRBs                                                                                                 
       for(int i = 0; i < 4; i++){// loop over FPGA                                                                                                                            
	 status = trb_register_clearbit(fpga_peri_ids[j][i],0xc801,0x80000000);
	 if(status == -1){
	   cm_msg(MERROR,"setup_trb","Failed to set TRB3 TDC registers; lost communication; TRB3 crashed (clock loss?)");
	   return -1;
	 }
       }
     }
  }

  // enable/disable particular FPGA channels.
  for(unsigned int j = 0; j < fpga_peri_ids.size(); j++){ // loop over TRBs
    
    for(int i = 0; i < 4; i++){// loop over FPGA

      //      std::string path2;
      char odbname[256];
      sprintf(odbname,"%s/Enable/fpga_0x%04x",path.c_str(),fpga_peri_ids[j][i]);
      BOOL fpga_enable[4] = {true,true,true,true};
      size = sizeof(fpga_enable);
      //      status = db_get_value(hDB, 0, path2.c_str(), &fpga_enable, &size, TID_BOOL, TRUE);
      status = db_get_value(hDB, 0, odbname, &fpga_enable, &size, TID_BOOL, TRUE);
            
      printf("Enable for board=%i, FPGA=%i, fpga ID=0%04x: ",j,i,fpga_peri_ids[j][i]);
      for(int k = 0; k < 4; k++){
        printf("%i ",fpga_enable[k]);
        setEnableBlock(fpga_peri_ids[j][i], k, fpga_enable[k]);
      }
      printf("\n");      
    }
  }



  cm_msg(MINFO,"frontend_init","TRB3 Board Setup Finished");
  
  return 0;
}

int frontend_init()
{
   int status;

   status = cm_get_experiment_database(&hDB, NULL);
   if (status != CM_SUCCESS) {
      cm_msg(MERROR, "frontend_init", "Cannot connect to ODB, cm_get_experiment_database() returned %d", status);
      return FE_ERR_ODB;
   }



   // Configure the socket connections:
   for(int i = 0; i < NTRBs; i++){
     udp_sockets.emplace_back(i);
     //udp_sockets[i].OpenUdpSocket(50246);
   }
   udp_sockets[0].OpenUdpSocket(50000);
   udp_sockets[1].OpenUdpSocket(50001);
   udp_sockets[2].OpenUdpSocket(50002);
   udp_sockets[3].OpenUdpSocket(50003);
   udp_sockets[4].OpenUdpSocket(50004);


   // Setup trbnet connection.  
   init_ports(); 
   
   // Check the FPGA addresses
   int i;
   uint16_t trb_address;
   trb_address = 0xffff;
   cm_msg(MINFO,"frontend_init","Reading addresses of TRB3s (central FPGA + 4 TDC FPGAs:");
   
   status = trb_read_uid(trb_address, buffer, BUFFER_SIZE);

   if (status == -1) {    
     cm_msg(MERROR,"frontend_init","TRB3 TDC: read_uid failed.  TRB3 not communicating.\n");
     return -1;     
   } else {      
      for (i = 0; i < status; i += 4) {     
	 fpga_all_ids.push_back(buffer[i+3]);

	 int id = buffer[i+3];

	 // Get the firmware version number
	 int status;
	 status= trb_register_read(id,0x40,buffer,BUFFER_SIZE);
	 char firmware_date[100];
	 if(status != -1){
	   unsigned int compile_time = buffer[1];
	   time_t ctime = compile_time;
	   struct tm * timeinfo;
	   timeinfo = localtime(&ctime);
	   char date_string[100];
	   strftime(date_string, 50, "%Y-%m-%d %H:%M", timeinfo);
	   sprintf(firmware_date," firmware date: %s",date_string);
	 }
         cm_msg(MINFO,"frontend_init","board = 0x%04x  0x%08x%08x  0x%02x  %s",           
                buffer[i + 3],
                buffer[i], buffer[i + 1], buffer[i + 2], firmware_date);

	 // Keep separate list of all non-central FPGAs	
	 if(buffer[i+2] == 5){
	   fpga_peri_ids.push_back(std::vector<int>());
	 }else{
	   fpga_peri_ids[fpga_peri_ids.size()-1].push_back(buffer[i+3]);
	 }

      }   
   }     
   
   // sort the arrays of IDs since not in order to start
   for(unsigned int j = 0; j < fpga_peri_ids.size(); j++){
     std::sort(fpga_peri_ids[j].begin(), fpga_peri_ids[j].end()); 
     //std::cout << "IDs for TRB " << j << std::endl;
     for(unsigned int i = 0; i < fpga_peri_ids[j].size(); i++){
       printf("IDs: 0x%x\n",fpga_peri_ids[j][i]);
     }
   }


   // Configure TRB3
   status = setup_trb();
   if(status != 0) return FE_ERR_HW;

   // Check the clock status 
   for(int i = 0; i < MAXTRB; i++){

     //Get central FPGA ID
     int id = fpga_all_ids[i*5];
     printf("%i %i\n",i,id);
     
     status= trb_register_read(id,0xd300,buffer,BUFFER_SIZE);
     unsigned int clock_status = (buffer[1] & 0x100) ; // Check bit 8; 1 = external clock, 0 = internal clock                                 
     if(clock_status){
       cm_msg(MINFO, "frontend_init", "TRB3 TDC 0x%04x is using an external clock.", id);
     }else{
       cm_msg(MINFO, "frontend_init", "TRB3 TDC 0x%04x is using an internal clock.", id);
     }
   }



  // Disable the external trigger on TRB3 (specifically extclk[0]):
  status = system("trbcmd clearbit 0xc001 0xa101 0x10");
  // Disable the software trigger on TRB3:
  //status = system("trbcmd clearbit 0xc001 0xa101 0x2");
  printf("Disabling TRB3 trigger %i\n",status);

  // Set the CPU affinity
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(5, &mask);  //Main thread to core 5
  if( sched_setaffinity(0, sizeof(mask), &mask) < 0 ){
    printf("ERROR setting cpu affinity for main thread: %s\n", strerror(errno));
  }

  cm_msg(MINFO, "frontend_init", "Finished Initializating TRB3 Frontend ");
  return SUCCESS;
}

int frontend_loop()
{
   ss_sleep(1);
   return SUCCESS;
}

pthread_t tid[MAXTRB]; 
int thread_retval[MAXTRB] = {0};
int ring_buffer_pointers[MAXTRB];
int thread_link[MAXTRB];

int counters[MAXTRB];

// Initial timestamp differences (relative to TRB0) at the start of each run
int initialTimeDiff[MAXTRB] = {0};
// Current timestamp differences (relative to TRB0) 
int currentTimeDiff[MAXTRB] = {0};


/* Swap bytes in 32 bit value.  */
#define R__bswap_constant_32(x) \
     ((((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >>  8) |               \
      (((x) & 0x0000ff00) <<  8) | (((x) & 0x000000ff) << 24))

void * link_thread(void * arg)
{
  int link = *(int*)arg;
  std::cout << "Started thread for link " << link << " out of 4 cores" << std::endl;

  //Lock each thread to a different cpu core
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET((link + 1), &mask);
  printf("core setting: link:%d core %d\n", link,(link + 1));
  if( sched_setaffinity(0, sizeof(mask), &mask) < 0 ){
    printf("ERROR setting cpu affinity for thread %d: %s\n", link, strerror(errno));
  }

  void *wp;
  int status;
  int rb_handle;
  int moduleID;
  int rb_level;

  while(1) {  // Indefinite until run stopped (!runInProgress)
    // This loop is running until EOR flag (runInProgress)


    rb_handle = udp_sockets[link].GetRingBufferHandle();
    moduleID=link;

    struct timeval start, mid1, mid2, end;

    gettimeofday(&start, NULL);

    // Check if UDP data to readout
    //    int length = read_udp(gDataSocket, buf, MAX_UDP_SIZE);
    bool have_udp = udp_sockets[link].CheckUdp(10);




    //    if (length > 0){
    if(have_udp && runInProgress){
      //if (length <= 0)
      //continue;


      
      /* If we've reached 75% of the ring buffer space, don't read
       * the next event.  Wait until the ring buffer level goes down.
       * It is better to let the v1725 buffer fill up instead of
       * the ring buffer, as this the v1725 will generate the HW busy to the
       * DTM.
       */
      rb_get_buffer_level(rb_handle, &rb_level);
      //      std::cout << ".";
      counters[link]++;
      if(counters[link] % 10000000 == 0)
	std::cout << "Ring buffer level ("<<link<<"): " << (float)rb_level /(float)event_buffer_size << std::endl;
      if(rb_level > (int)(event_buffer_size*0.75)) {

	if(counters[link] % 100000 == 0)
	  std::cout << "!!!Ring buffer level ("<<link<<"): " << (float)rb_level /(float)event_buffer_size << std::endl;

	continue;
      }



      
      // Ok to read data
      status = rb_get_wp(rb_handle, &wp, 100);
      if (status == DB_TIMEOUT) {
	cm_msg(MERROR,"link_thread", "Got wp timeout for thread %d (module %d).  Is the ring buffer full?",
	       link, moduleID);
	cm_msg(MERROR,"link_thread", "Exiting thread %d with error", link);
	thread_retval[link] = -1;
	pthread_exit((void*)&thread_retval[link]);
      }

      gettimeofday(&mid1, NULL);




      // Read data
      //      if(itv1725_thread[link]->ReadEvent(wp)) {
      if(udp_sockets[link].ReadEvent(wp)) {
      } else {
	cm_msg(MERROR,"link_thread", "Readout routine error on thread %d (module %d)", link, moduleID);
	cm_msg(MERROR,"link_thread", "Exiting thread %d with error", link);
	thread_retval[link] = -1;
	pthread_exit((void*)&thread_retval[link]);
      }

      gettimeofday(&mid2, NULL);

      

    }
    // Sleep for 5us to avoid hammering the board too much
    //    usleep(1);

    gettimeofday(&end, NULL);
    
    long seconds = (end.tv_sec - start.tv_sec);
    long micros = ((seconds * 1000000) + end.tv_usec) - (start.tv_usec);
    long seconds1 = (mid1.tv_sec - start.tv_sec);
    long micros1 = ((seconds1 * 1000000) + mid1.tv_usec) - (start.tv_usec);
    long seconds2 = (mid2.tv_sec - start.tv_sec);
    long micros2 = ((seconds2 * 1000000) + mid2.tv_usec) - (start.tv_usec);
    
    if(end.tv_usec %50000 == 0)    
      printf("Time elpased in main thread is %li %li %li micros; %i\n", micros1, micros2, micros, have_udp);
    


    
    // Escape if run is done -> kill thread
    if(!runInProgress)
      break;
  }

  std::cout << "Exiting thread " << link << " clean " << std::endl;
  thread_retval[link] = 0;
  pthread_exit((void*)&thread_retval[link]);
}


bool  gTemperatureReadFailed  = false;

int begin_of_run(int run_number, char *error)
{

  gTemperatureReadFailed = false;
  int status = setup_trb();
  if(status != 0) return FE_ERR_HW;
  
  //  setup_trb();
  
  
  // Setup the multi-threaded UDP readout
  for(int i = 0; i < NTRBs; i++){

    // Reset sanity check variables;                                                                                                                                           
    udp_sockets[i].begin_of_run();
    
    //Create ring buffer for each board
    int rb_handle;
    status = rb_create(event_buffer_size, max_event_size, &rb_handle);
    if(status == DB_SUCCESS){
      ring_buffer_pointers[i] = rb_handle;
      udp_sockets[i].SetRingBufferHandle(rb_handle);
    }
    else{
      cm_msg(MERROR, "TRBMulti:BOR", "Failed to create rb for board %d", i);
    }
    
    // Initialize a thread for each board
    thread_link[i] = i;
    status = pthread_create(&tid[i], NULL, &link_thread, (void*)&thread_link[i]);
    if(status){
      cm_msg(MERROR,"feov1725:BOR", "Couldn't create thread for link %d. Return code: %d", i, status);
    }
        
  }

  // Reset counters:
  numberBadTriggerMatch = 0;
  for(int i = 0; i < MAXTRB; i++){
    initialTimeDiff[i] = -9999;
    currentTimeDiff[i] = 0;
  }



  // Set the threads to start checking UDP buffers
  runInProgress = true;

  // Enable the external  (specifically extclk[0]) and software trigger on TRB3:
  status = system("trbcmd setbit 0xc001 0xa101 0x10");
  //status = system("trbcmd setbit 0xc001 0xa101 0x2");
  printf("Enabling TRB3 trigger %i\n",status);

  
  //status = system("trbcmd setbit   0xffff 0xc800 0x100");
  //status = system("trbcmd clearbit   0xffff 0xc800 0x100");


  return SUCCESS;
}
 #include <unistd.h>
#define MAX_UDP_SIZE (0x10000)
int end_of_run(int run_number, char *error)
{


  // Disable the external  (specifically extclk[0]) and software trigger on TRB3:
  int status = system("trbcmd clearbit 0xc001 0xa101 0x10");
  //status = system("trbcmd clearbit 0xc001 0xa101 0x2");
  printf("Disabling TRB3 trigger %i\n",status);

  // Wait for a little while to let the threads clean all the events out of
  // the UDP buffers (so they aren't sitting around for the next run.
  usleep(200000);

  if(runInProgress){  //skip actions if we weren't running
    
    runInProgress = false;  //Signal threads to quit
    
    
    // Do not quit parent before children processes, wait for the proper
    // child exit first.
    int *status2;
    for(int i = 0; i < NTRBs; i++){
      pthread_join(tid[i],(void**)&status2);
      printf(">>> Thread %d joined, return code: %d\n", i, *status2);
      rb_delete(ring_buffer_pointers[i]);
      if(udp_sockets[i].GetNumEventsInRB() > 0)
	cm_msg(MINFO,"end_of_run","TRB (#%i) number of events left in ring buffer: %i\n",i,udp_sockets[i].GetNumEventsInRB());
      udp_sockets[i].end_of_run();
    }
    
  }
 
  return SUCCESS;
}

int pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

int resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

int frontend_exit()
{

  // Disable the external trigger on TRB3:
  int status = system("trbcmd clearbit 0xc001 0xa101 0x40");
  printf("Disabling TRB3 trigger %i\n",status);

   return SUCCESS;
}

INT poll_event(INT source, INT count, BOOL test)
{

  for (int i = 0; i < count; i++) {
    
    //ready for readout only when data is present in all ring buffers
    bool evtReady = true;
    for(int j = 0 ; j < NTRBs; j++){
      if((udp_sockets[j].GetNumEventsInRB() == 0)) {
        evtReady = false;
      }
    }
    //If event not ready or we're in test phase, keep looping
    if (evtReady && !test)
      return 1;
    
    usleep(1);
  }

   return 0;
}
//#define MAX_UDP_SIZE (0x10000)

/* Swap bytes in 32 bit value.  */
#define R__bswap_constant_32(x) \
     ((((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >>  8) |               \
      (((x) & 0x0000ff00) <<  8) | (((x) & 0x000000ff) << 24))

int merge_trb_fragments(char *pevent, int off)
{

  if (!runInProgress) return 0;
  
  
  bk_init32(pevent);


  // Get TRB3 data from ring buffers  
  // Do matching of event number and time
  std::pair<uint32_t, uint32_t> first_info; 
  for(int i = 0; i < NTRBs; i++){    
    std::pair<uint32_t,uint32_t> trig_info = udp_sockets[i].FillEventBank(pevent);
    if(i == 0){
      first_info = trig_info;
    }
    
    //if(first_info.second %10000 == 0)
    // std::cout << "time check" << first_info.second << " " << trig_info.second << " " 
    //		<< first_info.second - trig_info.second << std::endl;
    
    if((trig_info.first != first_info.first)){ // Check if event numbers don't match
      numberBadTriggerMatch++;
      //     std::cout << "**Match error " << first_info.first << " " << trig_info.first << std::endl;
      
      if(numberBadTriggerMatch < 21){
	cm_msg(MERROR,"merge_trb_fragments","TRB (#%i) trigger number does not match TRB0: %i != %i\n",i,first_info.first,trig_info.first);	  
      }else if (numberBadTriggerMatch == 21){
	cm_msg(MERROR,"merge_trb_fragments","More than 20 trigger number mismatches seen when coming event.  Suppressing further messages");	  
      }
    }
    
    // Check timestamp differences
    int time_difference = ((int)trig_info.second) - ((int)first_info.second);
    if(initialTimeDiff[i] == -9999) initialTimeDiff[i] = time_difference;
    currentTimeDiff[i] =  time_difference - initialTimeDiff[i];    
    
  }
  

  return bk_size(pevent); 
}



// Read the TRB3 temperatures from trbnet interface
INT read_trb3_temperature(char *pevent, INT off){
  
  // Don't keep reading is the a previous temperature read failed.
  if(gTemperatureReadFailed) return 0;

  /* init bank structure */
  bk_init32(pevent);
  
  float *pdata;
  bk_create(pevent, "TRTM", TID_FLOAT, (void **)&pdata);

  // Loop over all FPGAs
  std::cout << "Temperatures: " ;
  for(unsigned int i = 0; i < fpga_all_ids.size(); i++){

    int status = trb_register_read(fpga_all_ids[i],0,buffer,BUFFER_SIZE);
    if(status == -1){
      cm_msg(MERROR, "read_trb3_temperature", "Failed to get TRB3 temperature with board (0x%x); lost communication; TRB3 crashed (clock loss?)",fpga_all_ids[i]);    
      gTemperatureReadFailed = true;
      return 0;    
    }
    float temp = (float)((buffer[1] & 0xfff00000) >> 20)/16.0;
    *pdata++ = temp;
    std::cout << temp << " ";
    if(i==0) printf("CTS FPGA temperature: %f\n",temp);
  
  }
  std::cout << std::endl;

  bk_close(pevent, pdata );    

  // Save another bank with ring buffer levels.   
  int rb_level;
  float *pdata2;
  bk_create(pevent, "TRRB", TID_FLOAT, (void **)&pdata2);
  
  for(int link = 0; link < NTRBs; link++){
    int rb_handle = udp_sockets[link].GetRingBufferHandle();
    int status =  rb_get_buffer_level(rb_handle, &rb_level);
    float frb_level = (float)rb_level /(float)event_buffer_size;
    //std::cout << link << " " << status << " " << frb_level << std::endl;
    *pdata2++ = frb_level;    
  }
  bk_close(pevent, pdata2 );    

  // Save third bank with time stamp differences.   
  int *pdata3;
  bk_create(pevent, "TRTS", TID_INT, (void **)&pdata3);
  
  for(int link = 0; link < NTRBs; link++){
    *pdata3++ = currentTimeDiff[link];
    std::cout << "timestamp diff: " << currentTimeDiff[link] << " "<< link << std::endl;
  }
  for(int link = 0; link < NTRBs; link++){
    *pdata3++ = initialTimeDiff[link];
  }

  bk_close(pevent, pdata3 );    

  return bk_size(pevent);
}


// Read the Padiwa thresholds
INT read_padiwa(char *pevent, INT off){
  
  /* init bank structure */
  bk_init32(pevent);
  
  // Don't keep reading is the a previous temperature read failed.
  if(gTemperatureReadFailed) return 0;

  // Loop over TRBS
  
  for(unsigned int j = 0; j < fpga_peri_ids.size(); j++){
    //   std::sort(fpga_peri_ids[j].begin(), fpga_peri_ids[j].end()); 
    //std::cout << "IDs for TRB " << j << std::endl;
    //for(unsigned int i = 0; i < fpga_peri_ids[j].size(); i++){
    // printf("IDs: 0x%x\n",fpga_peri_ids[j][i]);
    //}
    //}
    
    // skip all the FPGAs from first TRB... non-padiwa
    if(j == 0) continue;

    DWORD *pdata;
    int status;
    char bankname[5];
    sprintf(bankname,"PAD%i",j);
    bk_create(pevent, bankname, TID_DWORD, (void **)&pdata);

    float temperatures[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        
    // Loop over FPGAs; 
    // The data packet will be 256 words; one threshold reading for each of 64 channels per FPGA
    for(int i = 0; i < 4; i++){
      //    status = trb_register_read(fpga_ids[i],0,buffer,BUFFER_SIZE);
      
      // Check if this FPGA supports Padiwas
      status = trb_register_write(fpga_peri_ids[j][i],0xd410,1);
      if(trb_errno != 0){
	for(int i = 0; i < 64; i++) *pdata++ = 0;
	continue;
	std::cout << "Error writing to FPGA ID " << fpga_peri_ids[j][i] << std::endl;
      }
      //printf("Status (%i, %x) of 0xd410 write: %i %i\n",i,fpga_ids[i],status,trb_errno);
      
      // Loop over the padiwas
      for(int pad = 0; pad < 4; pad++){
	
	
	// Set the chain number for SPI interface
	status = trb_register_write(fpga_peri_ids[j][i],0xd410,1 << pad);      
	
	// Loop over channels
	for(int ch = 0; ch < 16; ch++){
	  
	  // Set the command for SPI interface for this channel
	  status = trb_register_write(fpga_peri_ids[j][i],0xd400, ch << 16);
	  
	  // Initiate the SPI command
	  status = trb_register_write(fpga_peri_ids[j][i],0xd411, 1);
	  
	  // Read back the threshold
	  status = trb_register_read(fpga_peri_ids[j][i],0xd412,buffer,BUFFER_SIZE);
	   
	  uint32_t threshold = buffer[1] & 0xffff;
	  //	  if(pad == 0 and ch == 0) printf("Threshold (0x%x,%i,%i): 0x%x  (%i %i) \n",fpga_peri_ids[j][i],pad,ch,threshold,status, errno);
	  
	  *pdata++ = threshold;
	  
	}
	
	// Grab temperature
	
	// Write the command into memory
	uint32_t commands[18] = {0x10040000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1<<pad,1};
	status = trb_register_write_mem(fpga_peri_ids[j][i],0xd400,0,commands,sizeof(commands));

	//printf("status mem %i %i %i %i\n",j,i,pad,status);

	// setup for this chain
	status = trb_register_write(fpga_peri_ids[j][i],0xd410,1 << pad);

	// Initiate the SPI command                                                                                                                  
	status = trb_register_write(fpga_peri_ids[j][i],0xd411, 1);

	usleep(1);
	
	// read back temp
	status = trb_register_read(fpga_peri_ids[j][i],0xd412,buffer,BUFFER_SIZE);
	uint32_t temp = buffer[1];
	//if(pad == 0) printf("Padiwa Temp (%i %i %i): %f\n",j, i, pad,((float)(temp & 0xfff))/16.0);
	
	if(pad < 3) temperatures[pad + i*4] = ((float)(temp & 0xfff))/16.0;

	usleep(1);
      }    
      usleep(100);
    }
    
    bk_close(pevent, pdata );    

    // Now save the temperatures
    float *pdata2;
    sprintf(bankname,"PTM%i",j);
    bk_create(pevent, bankname, TID_FLOAT, (void **)&pdata2);
    for(int tt = 0; tt < 16; tt++){
      *pdata2++ = temperatures[tt];      
    }
    bk_close(pevent, pdata2);    


  }
 
 
  return bk_size(pevent);
}

